..
    Copyright (C) 2017-2018 HZDR

    This file is part of Rodare.

    Rodare is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rodare is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

Changes
=======

Version 0.2.3 (released 2022-04-06)
-----------------------------------

- The elasticsearch version can now be chosen by installing one of the package extras ``elasticsearch{2,5,6,7}``.
  (`!74 <https://codebase.helmholtz.cloud/rodare/invenio-uploadbyurl/-/merge_requests/74>`_)

Version 0.2.2 (released 2022-02-15)
-----------------------------------

- Settings: A handler for deleting a ssh key was created and the private key must now added when calling the deploy_ssh_key function.
  (`b7be19f0 <https://codebase.helmholtz.cloud/rodare/invenio-uploadbyurl/-/commit/b7be19f0f12c9662e171dd6c807024f7b00ccc75>`_)

Version 0.2.1 (released 2018-12-10)
-----------------------------------

- Settings: Improve Layout on small screens by adding padding around new
  deposit info box in settings view.
  (`!64 <https://gitlab.hzdr.de/rodare/invenio-uploadbyurl/merge_requests/64>`_)

Version 0.2.0 (released 2018-11-28)
------------------------------------

- Settings: Job Status UI that displays information about the jobs status for
  the current user (`!60 <https://gitlab.hzdr.de/rodare/invenio-uploadbyurl/merge_requests/60>`_)
- REST API to retrieve active background jobs
  (`!56 <https://gitlab.hzdr.de/rodare/invenio-uploadbyurl/merge_requests/56/diffs>`_)
- Build docs with latest Sphinx version
  (`!61 <https://gitlab.hzdr.de/rodare/invenio-uploadbyurl/merge_requests/61/diffs>`_)
- Test improvements
  (`!59 <https://gitlab.hzdr.de/rodare/invenio-uploadbyurl/merge_requests/59/diffs>`_)

Version 0.1.1 (released 2018-10-12)
-----------------------------------

- Settings: fix views being accessible without login
  (`!58 <https://gitlab.hzdr.de/rodare/invenio-uploadbyurl/merge_requests/58>`_)

**Please upgrade to the latest version immediately.**

Version 0.1.0 (released 2018-10-02)
-----------------------------------

- Initial public release.
