..
    Copyright (C) 2017-2018 HZDR

    This file is part of Rodare.

    Rodare is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rodare is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

Installation
============

Invenio-UploadByURL is not yet available via PyPI. Nevertheless,
it can be installed via PIP.

.. code-block:: console

   $ pip install -e git+https://gitlab.hzdr.de/rodare/invenio-uploadbyurl.git@<tag>#egg=invenio-uploadbyurl

Build the docs
--------------

To build the html version of the documentation follow the steps below:

.. code-block:: bash

    $ git clone git@gitlab.hzdr.de:rodare/invenio-uploadbyurl.git
    $ cd invenio-uploadbyurl
    # suggestion: switch to virtual environment before
    $ pip install -e .[all]
    $ cd docs && make html

The built documentation can be found in ``docs/_build/html``.
