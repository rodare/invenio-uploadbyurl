..
    Copyright (C) 2017-2019 HZDR

    This file is part of Rodare.

    Rodare is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rodare is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

=====================
 Invenio-UploadByURL
=====================

.. image:: https://gitlab.hzdr.de/rodare/invenio-uploadbyurl/badges/master/pipeline.svg
        :target: https://gitlab.hzdr.de/rodare/invenio-uploadbyurl/commits/master

.. image:: https://gitlab.hzdr.de/rodare/invenio-uploadbyurl/badges/master/coverage.svg
        :target: https://gitlab.hzdr.de/rodare/invenio-uploadbyurl/commits/master

Module for invenio that integrates server-side download from a given URL in asynchronous tasks.

Developer documentation
=======================

Documentation is hosted via Gitlab Pages: http://rodare.pages.hzdr.de/invenio-uploadbyurl/
