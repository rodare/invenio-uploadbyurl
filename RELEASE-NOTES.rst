..
    Copyright (C) 2017-2018 HZDR

    This file is part of Rodare.

    Rodare is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rodare is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

============================
 Invenio-UploadByURL v0.1.0
============================

Invenio-UploadByURL v0.1.0 was released on TBD, 2017.

About
-----

Module for invenio that integrates server-side download from a given URL in asynchronous tasks.

*This is an experimental developer preview release.*

What's new
----------

- Initial public release.

Installation
------------

   $ pip install invenio-uploadbyurl==0.1.0

Documentation
-------------

   https://invenio-uploadbyurl.readthedocs.io/

Happy hacking and thanks for flying Invenio-UploadByURL.
