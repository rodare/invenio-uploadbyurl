..
    Copyright (C) 2017-2018 HZDR

    This file is part of Rodare.

    Rodare is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rodare is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rodare.  If not, see <http://www.gnu.org/licenses/>.


API Docs
========

.. automodule:: invenio_uploadbyurl.ext
   :members:

API
----

.. automodule:: invenio_uploadbyurl.api
   :members:

Bundles
-------

.. automodule:: invenio_uploadbyurl.bundles
   :members:

Errors
------

.. automodule:: invenio_uploadbyurl.errors
   :members:

Forms
-----

.. automodule:: invenio_uploadbyurl.forms
   :members:

Links
-----

.. automodule:: invenio_uploadbyurl.links
   :members:

Models
------

.. automodule:: invenio_uploadbyurl.models
   :members:

Receivers
---------

.. automodule:: invenio_uploadbyurl.receivers
   :members:

Signals
-------

.. automodule:: invenio_uploadbyurl.signals
   :members:

Tasks
-----

.. automodule:: invenio_uploadbyurl.tasks
   :members:

Utils
-----

.. automodule:: invenio_uploadbyurl.utils
   :members:

Views
-----

.. automodule:: invenio_uploadbyurl.views
   :members:

.. automodule:: invenio_uploadbyurl.views.settings
   :members: