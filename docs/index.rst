..
    Copyright (C) 2017-2018 HZDR

    This file is part of Rodare.

    Rodare is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Rodare is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Rodare.  If not, see <http://www.gnu.org/licenses/>.


.. include:: ../README.rst

User's Guide
------------

This part of the documentation will show you how to get started in using
Invenio-UploadByURL.

.. toctree::
   :caption: User's Guide
   :maxdepth: 2

   installation
   configuration
   usage
   examplesapp


API Reference
-------------

If you are looking for information on a specific function, class or method,
this part of the documentation is for you.

.. toctree::
   :caption: API Reference
   :maxdepth: 2

   api

Additional Notes
----------------

Notes on how to contribute, legal information and changes are here for the
interested.

.. toctree::
   :caption: Additional Notes
   :maxdepth: 1

   contributing
   changes
   license
   authors
