#!/bin/sh
# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

# quit on errors:
set -o errexit

# quit on unbound symbols:
set -o nounset

DIR=`dirname "$0"`

cd $DIR
export FLASK_APP=app.py

# Setup fixtures

# Create a user
flask users create test@hzdr.de -a --password 123456

# Load some test data
flask fixtures files