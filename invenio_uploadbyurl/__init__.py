# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

r"""
Module for invenio for server-side download from a given URL.

The invenio-uploadyburl module for Invenio adds the ability to upload
files from a remote server via the available protocols. Currently, SFTP and
HTTP are implemented. The downloads are performed in asynchronous celery
tasks. The user is notified via Email, once the task is finished or has failed
for some reason.

Alembic
-------

If you wish to add invenio-uploadbyurl to an existing installation, you can
use the provided alembic recipes to migrate your database.

In general you need to invoke the following command to migrate your db:

.. code-block:: python

    invenio alembic upgrade heads

Further information is available in the documentation of
`Invenio-DB <https://invenio-db.readthedocs.io/en/latest/alembic.html>`_.

Initialization
--------------

For using the SFTP background upload you need to register remote servers
first. If you wish to register the server with the address
``sftp1.test.de`` accessible on port 22, execute the following lines of code
in the application context:

.. code-block:: python

    from invenio_db import db
    from invenio_uploadbyurl.models import RemoteServer

    remote = RemoteServer(
        name='sftp1',
        server_address='sftp1.test.de',
        port=22,
        description='SFTP test server.',
    )
    db.session.add(remote)
    db.session.commit()

Before you try to connect to the registered RemoteServers, make sure that the
RemoteServer's host key is added to the system host keys of your operating
system. Otherwise, all connection attempts will fail.

Customization
-------------

For some SFTP servers it might be necessary to overwrite the default functions
for e.g. key deployment. It can be configured by specifing the configuration
variable ``UPLOADBYURL_REMOTE_SERVERS``.

Example
^^^^^^^
.. code-block:: python

    UPLOADBYURL_REMOTE_SERVERS = {
        'mysftp': {
            'deploy_handler': 'mymodule.utils:deploy_ssh_key',
            'delete_handler': 'mymodule.utils:delete_ssh_key',
        }
    }

More customization options are planned for future releases.

REST API
--------

Background uploads can be triggered via the REST API. Many operations operate
on a bucket, defined by
`Invenio-Files-REST  <https://invenio-files-rest.readthedocs.io/
en/latest/index.html>`_.
For the operations presented here, it is required to connect the user account
with the remote server before. You are also required to be authenticated.

Let's assume the bucket we operate on has the UUID defined in the variable
bucket:

.. code-block:: bash

    $ export BUCKET=11111111-1111-1111-1111-111111111111

Furthermore the remote server with the name ``sftp1`` is registered.

.. code-block:: bash

    $ export SERVER_NAME=11111111-1111-1111-1111-111111111111

The following REST API endpoints can then be used.

File Browsing
^^^^^^^^^^^^^

Browse root directory of remote server ``sftp1``:

.. code-block:: bash

    curl -X POST http://localhost:5000/uploadbyurl/browse/$SERVER_NAME

The response could look similar to the following JSON object:

.. literalinclude:: static/browse.json
    :language: json

By adding the path parameter you can browse a specific directory, e.g.:

.. code-block:: bash

    curl -X POST http://localhost:5000/uploadbyurl/browse/sftp1?path=%2Fhome

and receive a similar json response. The file browsing results are cached for
300s to reduce the amount of traffic on the remote servers.

SFTP background upload
^^^^^^^^^^^^^^^^^^^^^^

Background uploads via SFTP can be triggered via the REST API. Let's assume
we wish to upload the file ``/home/foo/foo``. Therefore, the following request
needs to be performed:

.. code-block:: bash

    $ curl -X POST http://localhost:5000/uploadbyurl/$BUCKET/ \\
        $SERVER_NAME?path=/home/foo/foo

Upload via URL
^^^^^^^^^^^^^^

Uploading via URL can be triggered via an HTTP request, too:

.. code-block:: bash

    $ curl -X POST http://localhost:5000/uploadbyurl/$BUCKET \\
        ?url=http://ipv4.download.thinkbroadband.com/5MB.zip

"""

from .ext import InvenioUploadByURL
from .proxies import current_uploadbyurl
from .version import __version__

__all__ = (
    '__version__',
    'current_uploadbyurl',
    'InvenioUploadByURL',
)
