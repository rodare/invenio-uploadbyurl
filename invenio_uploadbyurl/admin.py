# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Administration interface for invenio-uploadbyurl."""

from flask_admin.contrib.sqla import ModelView
from flask_babelex import gettext as _
from invenio_accounts.models import User

from .models import RemoteServer, SSHKey


class RemoteServerAdmin(ModelView):
    """Remote Server admin view."""

    can_create = True
    can_edit = True
    can_delete = True
    can_view_details = True

    column_list = ('name', 'server_address', 'port', 'description')
    column_details_list = ('name', 'server_address', 'port', 'description')


class SSHKeyAdmin(ModelView):
    """SSH Key admin view."""

    can_create = False
    can_edit = False
    can_delete = False
    can_view_details = True

    column_list = ('username', 'keytype', 'user', 'remote_server',
                   'valid_until')
    column_details_list = ('username', 'keytype', 'user', 'remote_server',
                           'valid_until')
    column_searchable_list = ('username', User.email, )

    page_size = 25


remoteserver_adminview = dict(
    modelview=RemoteServerAdmin,
    model=RemoteServer,
    category=_('Background upload')
)

sshkey_adminview = dict(
    modelview=SSHKeyAdmin,
    model=SSHKey,
    category=_('Background upload')
)
