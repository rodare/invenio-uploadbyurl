# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Create invenio-uploadbyurl tables."""

from datetime import date

import sqlalchemy as sa
from alembic import op
from sqlalchemy_utils import EncryptedType

# revision identifiers, used by Alembic.
revision = '423477d42087'
down_revision = '941eb3f74cd1'
branch_labels = ()
depends_on = '9848d0149abd'


def upgrade():
    """Upgrade database."""
    op.create_table('uploadbyurl_remoteserver',
                    sa.Column(
                        'id',
                        sa.Integer(),
                        nullable=False,
                        autoincrement=True),
                    sa.Column('name', sa.String(length=20), nullable=False),
                    sa.Column('access_protocol', sa.String(length=32),
                              default='sftp', nullable=False),
                    sa.Column('server_address', sa.String(
                        length=22), nullable=False),
                    sa.Column('port', sa.Integer(),
                              default=22, nullable=False),
                    sa.Column('description', sa.Text(),
                              default='Empty description', nullable=False),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('name')
                    )

    op.create_table('uploadbyurl_sshkeys',
                    sa.Column('id', sa.Integer(), autoincrement=True),
                    sa.Column('private_key', EncryptedType(), nullable=False),
                    sa.Column('valid_until', sa.Date(),
                              default=date.min, nullable=False),
                    sa.Column('keytype', sa.String(length=10),
                              default='rsa', nullable=False),
                    sa.Column('username', sa.String(
                        length=20), nullable=False),
                    sa.Column('user_id', sa.Integer(), nullable=False),
                    sa.Column('remote_server_id',
                              sa.Integer(), nullable=False),
                    sa.ForeignKeyConstraint(['user_id'], ['accounts_user.id']),
                    sa.ForeignKeyConstraint(['remote_server_id'],
                                            ['uploadbyurl_remoteserver.id']),
                    sa.PrimaryKeyConstraint('id'),
                    sa.UniqueConstraint('user_id', 'remote_server_id')
                    )


def downgrade():
    """Downgrade database."""
    op.drop_table('uploadbyurl_sshkeys')
    op.drop_table('uploadbyurl_remoteserver')
