# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Alter length of column server_address."""

import sqlalchemy as sa
from alembic import op

# revision identifiers, used by Alembic.
revision = '8eb4749c8f4b'
down_revision = '423477d42087'
branch_labels = ()
depends_on = None


def upgrade():
    """Upgrade database."""
    op.alter_column(
        'uploadbyurl_remoteserver',
        'server_address',
        type_=sa.String(length=255),
        existing_nullable=False,
    )


def downgrade():
    """Downgrade database."""
    op.alter_column(
        'uploadbyurl_remoteserver',
        'server_address',
        type_=sa.String(length=22),
        existing_nullable=False,
    )
