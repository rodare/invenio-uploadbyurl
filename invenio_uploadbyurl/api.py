# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Upload by URL Rest API."""

import json
import os
import socket
import stat
from datetime import datetime
from io import StringIO
from urllib.parse import urlencode, urlparse

import humanize
import paramiko
import requests
from celery.result import AsyncResult
from flask import Blueprint, abort, current_app, jsonify, request, url_for
from flask_login import current_user, login_required
from invenio_files_rest.serializer import json_serializer
from invenio_files_rest.views import need_bucket_permission, pass_bucket
from invenio_rest import ContentNegotiatedMethodView
from webargs import fields
from webargs.flaskparser import use_kwargs
from werkzeug.local import LocalProxy

from .decorators import cached, need_authentication
from .errors import AuthenticationError, FileDoesNotExist, FileTooLargeError, \
    MissingPathError, MissingURLError, NoAbsolutePathError, NoFileError, \
    NoPathError, RemoteServerNotFoundError, RequestError, SSHException, \
    SSHKeyNotFoundError, UnsupportedProtocolError
from .models import RemoteServer, SSHKey
from .tasks import download_files, download_via_sftp
from .utils import append_cache_entry, create_cache_entry, create_cache_key, \
    limit

_redisstore = LocalProxy(
    lambda: current_app.extensions['invenio-uploadbyurl'].redisstore)

blueprint = Blueprint(
    'invenio_uploadbyurl',
    __name__,
    url_prefix='/uploadbyurl',
)


def make_key(*args, **kwargs):
    """Make cache key function for memoization."""
    path = request.path
    args = str(hash(frozenset(request.args.items())))
    user_id = str(current_user.id)
    return path + args + user_id


def url_validator(value):
    """Validates if the given URL is in the list of allowed domains.

    The validator also checks, if the URL is reachable and, if possible,
    tries to determine the filesize of the specified file.
    """
    parse_result = urlparse(value)
    allowed_protocols = current_app.config['UPLOADBYURL_ALLOWED_PROTOCOLS']
    # check if the used protocol scheme is allowed
    if parse_result.scheme not in allowed_protocols:
        raise UnsupportedProtocolError()
    # check if URL is valid and reachable
    try:
        r = requests.head(value, timeout=1)
    except requests.RequestException:
        raise RequestError()


def path_validator(value):
    """Validates if the given string is an absolute path."""
    if not os.path.isabs(value):
        raise NoAbsolutePathError()


class UploadByUrl(ContentNegotiatedMethodView):
    """Upload file Rest class."""

    get_args = {
        'celery_id': fields.UUID(
            location='query',
        )
    }

    post_args = {
        'url': fields.URL(
            location='query',
            validate=url_validator,
        ),
        'key': fields.Str(
            location='query',
            missing=None,
        ),
    }

    @use_kwargs(get_args)
    @pass_bucket
    @need_bucket_permission('bucket-update')
    def get(self, bucket, celery_id=None):
        """Get status of upload task.

        If the celery ID does not exist, res.state returns 'PENDING'.
        So Pending has two meanings.
        """
        state_dict = dict()
        try:
            key = create_cache_key(current_user.id)
            job_list = _redisstore.get(key)
            if job_list:
                data = json.loads(job_list.decode('utf-8'))
                for entry in filter(
                        lambda x: x['bucket_id'] == str(bucket.id), data):
                    celery_id = entry['celery_id']
                    res = AsyncResult(celery_id)
                    created = datetime.fromtimestamp(entry['created'])
                    state_dict[celery_id] = {
                        'bucket': entry['bucket_id'],
                        # Python 3.6+ allows more compact syntax:
                        # created.isoformat(timespec='seconds')
                        'created': created.replace(microsecond=0).isoformat(),
                        'location': entry['location'],
                        'protocol': entry['protocol'],
                        'state': res.state,
                    }
                if not state_dict:
                    return abort(
                        404, 'No upload task found for the given bucket.')
                return jsonify(state_dict)
        except KeyError:
            return abort(404, 'No upload task found for the given bucket.')

    @use_kwargs(post_args)
    @pass_bucket
    @need_bucket_permission('bucket-update')
    def post(self, bucket, url=None, key=None):
        """Create new object version from the file in the given URL."""
        if url:
            self.check_filesize(url, bucket)
            celery_id = download_files.delay(
                str(bucket.id),
                current_user.id,
                [url],
                key
            )
            # add celery_id to redisstore
            key = create_cache_key(current_user.id)
            entry = create_cache_entry(celery_id, bucket.id, url, 'http')
            append_cache_entry(key, entry)
            response = jsonify(
                celery_id=str(celery_id),
                message='Started file upload in background.',
                status=202,
            )
            response.status_code = 202
            return response
        raise MissingURLError()

    @staticmethod
    def check_filesize(url, bucket):
        """Check filesize of URL via Content-Length header."""
        try:
            r = requests.head(url, timeout=1)
            size = int(r.headers.get('content-length', 0))
            # check file size limits
            if (bucket.quota_left < size or
                    bucket.size_limit < size):
                raise FileTooLargeError()
        except requests.RequestException:
            pass


class SFTPBrowserAPI(ContentNegotiatedMethodView):
    """Browse files API via sftp."""

    post_args = {
        'path': fields.Str(
            location='query',
            validate=path_validator,
            missing='/',
        ),
        'max': fields.Integer(
            location='query',
            missing=10000,
        )
    }

    @use_kwargs(post_args)
    @need_authentication
    @cached(timeout=300, key_prefix=make_key, query_string=False)
    def post(self, remote_server, path='/', max=10000):
        """
        Return list of directories and files.

        Arguments:
            remote_server(str): The name of the RemoteServer that should be
                browsed for files.
            path(str): The contents of the specified path will be returned.

        Returns:
            flask.Response: a JSON response containing the elements of the
            directory specified in the ``path`` variable.

        """
        remote = RemoteServer.get_by_name(remote_server)
        if not remote:
            raise RemoteServerNotFoundError()
        key = SSHKey.get(current_user.id, remote.id)
        if not key:
            raise SSHKeyNotFoundError()
        pkey = current_app.config[
            'UPLOADBYURL_KEY_DISPATCH_TABLE'][key.keytype].from_private_key(
                StringIO(key.private_key))
        with paramiko.SSHClient() as client:
            try:
                client.load_system_host_keys()
                client.connect(
                    key.remote_server.server_address,
                    username=key.username,
                    pkey=pkey,
                    timeout=current_app.config['UPLOADBYURL_SHORT_TIMEOUT'],
                )
                sftp = client.open_sftp()
                dirlist = []
                base_url = url_for(
                    'invenio_uploadbyurl.uploadbyurl_api_sftp_browse',
                    remote_server=remote.name,
                    path='/',
                    _external=True,
                ).split('?')[0] + '?'
                if path != '/':
                    full_path = os.path.split(path)[0]
                    endpoint = base_url + urlencode({'path': full_path})
                    dirlist = [dict(
                        short_path='..',
                        path=full_path,
                        isdir=True,
                        endpoint=endpoint,
                    )]
                for element in limit(sftp.listdir_iter(path), limit=max):
                    # get file size
                    full_path = os.path.join(path, element.filename)
                    endpoint = base_url + urlencode({'path': full_path})
                    size = humanize.naturalsize(element.st_size, binary=True)
                    node = dict(
                        short_path=element.filename,
                        path=full_path,
                        isdir=stat.S_ISDIR(element.st_mode),
                        endpoint=endpoint,
                        size=size,
                    )
                    dirlist.append(node)
            except IOError:
                raise NoPathError()
            except paramiko.AuthenticationException:
                raise AuthenticationError()
            except (paramiko.SSHException, socket.error):
                raise SSHException()
        response = jsonify(dirlist)
        response.status_code = 200
        return response


class UploadViaSFTP(ContentNegotiatedMethodView):
    """Upload via sftp REST class."""

    post_args = {
        'path': fields.Str(
            location='query',
            validate=path_validator,
        ),
        'key': fields.Str(
            location='query',
            missing=None,
        ),
    }

    @use_kwargs(post_args)
    @pass_bucket
    @need_bucket_permission('bucket-update')
    def post(self, bucket, remote_server, path=None, key=None):
        """
        Create new object version from the file in the given path.

        Verify first, if the file is within the size limits, readable, etc.
        Download the file in an asynchronous celery task via SFTP.

        Arguments:
            bucket(str): The ID of the destination bucket.
            remote_server(str): The name of the RemoteServer to connect with.
            path(str): The absolute filepath to download the file from.
            key(str): An alternative key name for the generated file.

        Returns:
            flask.Response: Return JSON response with success information.

        Raises:
            invenio_uploadbyurl.errors.RemoteServerNotFoundError: if requested
                RemoteServer is not registered.
            invenio_uploadbyurl.errors.SSHKeyNotFoundError: if user account is
                not connected with the RemoteServer yet.
            invenio_uploadbyurl.errors.MissingPathError: if no path is given.

        """
        if path:
            # get remote
            remote = RemoteServer.get_by_name(remote_server)
            if not remote:
                raise RemoteServerNotFoundError()
            key = SSHKey.get(current_user.id, remote.id)
            if not key:
                raise SSHKeyNotFoundError()
            self.verify_request(remote, path, bucket)
            celery_id = download_via_sftp.delay(
                str(bucket.id), remote.id, current_user.id, path)

            # add celery_id to redisstore
            key = create_cache_key(current_user.id)
            entry = create_cache_entry(celery_id, bucket.id, path, 'sftp')
            append_cache_entry(key, entry)

            response = jsonify(
                message='Started file upload in background.',
                status=202,
            )
            response.status_code = 202
            return response
        raise MissingPathError()

    @staticmethod
    def verify_request(remote_server, filepath, bucket):
        """
        Verify request and return descriptive error message.

        Connect to the RemoteServer via SFTP and check if given path specifies
        a regular file, if the file is within the file size limits of the
        bucket.

        Arguments:
            remote_server(RemoteServer): The RemoteServer object.
            filepath(str): The path of a file on the RemoteServer.
            bucket(invenio_files_rest.models.Bucket):

        Raises:
            invenio_uploadbyurl.errors.NoFileError: if ``filepath`` does not
                specify a regular file.
            invenio_uploadbyurl.errors.FileTooLargeError:
            invenio_uploadbyurl.errors.FileDoesNotExist:
            invenio_uploadbyurl.errors.AuthenticationError:
            invenio_uploadbyurl.errors.SSHException:

        """
        key = SSHKey.get(current_user.id, remote_server.id)
        pkey = current_app.config[
            'UPLOADBYURL_KEY_DISPATCH_TABLE'][key.keytype].from_private_key(
                StringIO(key.private_key))
        # get filesize and check if is file via paramiko
        with paramiko.SSHClient() as client:
            try:
                client.load_system_host_keys()
                client.connect(
                    key.remote_server.server_address,
                    username=key.username,
                    pkey=pkey,
                    timeout=current_app.config['UPLOADBYURL_SHORT_TIMEOUT'],
                )
                sftp = client.open_sftp()
                file_stat = sftp.stat(filepath)
                if stat.S_ISREG(file_stat.st_mode):
                    size = sftp.stat(filepath).st_size
                else:
                    raise NoFileError()
                # check file size limits
                if (bucket.quota_left < size or
                        bucket.size_limit < size):
                    raise FileTooLargeError()
            except IOError:
                raise FileDoesNotExist()
            except paramiko.AuthenticationException:
                raise AuthenticationError()
            except (paramiko.SSHException, socket.error):
                raise SSHException()


upload_view = UploadByUrl.as_view(
    'uploadbyurl_api',
    serializers={
        'application/json': json_serializer,
    }
)

upload_sftp_view = UploadViaSFTP.as_view(
    'uploadbyurl_api_sftp',
    serializers={
        'application/json': json_serializer,
    }
)

upload_sftp_browse_view = SFTPBrowserAPI.as_view(
    'uploadbyurl_api_sftp_browse',
    serializers={
        'application/json': json_serializer,
    }
)

blueprint.add_url_rule(
    '/<string:bucket_id>',
    view_func=upload_view,
)

blueprint.add_url_rule(
    '/<string:bucket_id>/<string:remote_server>',
    view_func=upload_sftp_view,
)

blueprint.add_url_rule(
    '/browse/<string:remote_server>',
    view_func=upload_sftp_browse_view,
)
