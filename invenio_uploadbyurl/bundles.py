# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Uploadbyurl bundles."""

from flask_assets import Bundle
from invenio_assets import NpmBundle

js_dependencies_clipboard = NpmBundle(
    'node_modules/clipboard/dist/clipboard.js',
    npm={
        'clipboard': '~1.7.1',
    }
)

js_dependencies_jquery = NpmBundle(
    'node_modules/jquery/jquery.js',
    npm={
        'jquery': '~1.9.1',
    }
)

js_dependencies_ui = NpmBundle(
    'node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
    npm={
        'bootstrap-sass': '~3.3.5',
    }
)

js_dependencies = NpmBundle(
    js_dependencies_clipboard,
    js_dependencies_jquery,
    js_dependencies_ui,
    filters='jsmin',
    output='gen/uploadbyurl.dependencies.%(version)s.js',
)

js = Bundle(
    "js/invenio_uploadbyurl/main.js",
    filters='jsmin',
    output='gen/uploadbyurl.%(version)s.js',
)
