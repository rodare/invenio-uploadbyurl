# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Module for invenio for server-side download from a given URL."""


import pkg_resources
from paramiko import ECDSAKey, RSAKey

UPLOADBYURL_WHITELIST = []
"""Whitelist of addressed that are allowed."""

UPLOADBYURL_ALLOWED_PROTOCOLS = ['https']
"""Only allow download via the specified protocols."""

UPLOADBYURL_REDISSTORE_URL = 'redis://localhost:6379/0'
"""Redis URL."""

UPLOADBYURL_KV_PREFIX = 'invenio_uploadbyurl'
"""Prefix in redis store."""

UPLOADBYURL_KEY_DISPATCH_TABLE = {
    'rsa': RSAKey,
    'ecdsa': ECDSAKey
}
"""Supported SSH key types."""

UPLOADBYURL_ACTIVATE = True
"""Register settings entry in user menu."""

UPLOADBYURL_NOTIFICATION_ENABLED = False
"""Activate notification after finished upload."""
try:
    pkg_resources.get_distribution('flask_mail')
    UPLOADBYURL_NOTIFICATION_ENABLED = True
except pkg_resources.DistributionNotFound:  # pragma: no cover
    pass

UPLOADBYURL_COMMENT = 'key@uploadbyurl.de'
"""Name of key to be added in authorized_keys."""

UPLOADBYURL_TIMEOUT = 60 * 60  # 1 hour
"""Timeout in s after which upload task should be quit."""

UPLOADBYURL_SHORT_TIMEOUT = 15
"""Timeout in s after which short tasks, e.g. echo, should be quit."""

UPLOADBYURL_SENDER_EMAIL = 'rodare@hzdr.de'
"""Email address for sending mails."""

UPLOADBYURL_EMAIL_TITLE = 'Your upload is finished'
"""Message title used for notification email."""

UPLOADBYURL_EMAIL_BODY = 'Your upload finished successfully. You can now ' \
    'finish entering metadata for your record. Do not forget to finally ' \
    'publish your record.'
"""Message body used for notification email."""

UPLOADBYURL_EMAIL_TITLE_FAILED = 'Your upload failed'
"""Message title used for notification email in case of failure."""

UPLOADBYURL_EMAIL_BODY_FAILED = 'Your upload failed. Please make sure ' \
    'that the account you linked with the SFTP server has read permissions ' \
    'for {filepath}. Also make sure, that the file exists. Please contact ' \
    'us, if the problem persists.'
"""Message body used for notification mail in case of failure."""

UPLOADBYURL_EMAIL_BODY_FAILED_URL = 'Your upload failed. The file could ' \
    'not be downloaded successfully from {url}. Please contact us, if the ' \
    'problem persists.'
"""Message body used for notification mail in case of failure."""

UPLOADBYURL_REMOTE_SERVERS = {}
"""Special configuration option for remote server."""

UPLOADBYURL_REDIS_KEY_TTL = 7 * 24 * 3600
"""Time-To-Live value in seconds for the job ids stored in Redis."""

UPLOADBYURL_CACHE_DURATION = 7
"""Number of days after which cache entries shall be removed."""

UPLOADBYURL_USER_NOTIFICATIONS_OFF = []
"""List of user ids not being notified via E-Mail about the job status."""
