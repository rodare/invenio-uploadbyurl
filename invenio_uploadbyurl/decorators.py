# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Decorators for invenio-uploadbyurl."""

from functools import wraps

from flask import abort
from flask_login import current_user
from invenio_cache import current_cache, current_cache_ext


def cached(timeout=50, key_prefix='default', query_string=False):
    """Cache anonymous traffic."""
    def caching(f):
        @wraps(f)
        def wrapper(*args, **kwargs):
            cache_fun = current_cache.cached(
                timeout=timeout,
                key_prefix=key_prefix,
                query_string=query_string,
                unless=(lambda:
                        not current_cache_ext.is_authenticated_callback()),
            )
            return cache_fun(f)(*args, **kwargs)
        return wrapper
    return caching


def need_authentication(func):
    """Decorator for API call to check if user is authenticated."""
    @wraps(func)
    def decorated_api_view(*args, **kwargs):
        if not current_user.is_authenticated:
            abort(401)
        return func(*args, **kwargs)
    return decorated_api_view
