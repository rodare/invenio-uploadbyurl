# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""REST Errors for invenio-uploadbyurl."""

from flask import current_app
from invenio_rest.errors import RESTException


class RemoteServerNotFoundError(RESTException):
    """Remote Server not found."""

    code = 404
    description = 'Remote Server not found.'


class SSHKeyNotFoundError(RESTException):
    """Remote server and user are not connected yet."""

    code = 400
    description = 'Please connect your account with the remote server first.'


class MissingPathError(RESTException):
    """No path argument was given."""

    code = 400
    description = 'You need to specify an absolute file path.'


class MissingURLError(RESTException):
    """No URL argument was given."""

    code = 400
    description = 'You need to specify a URL.'


class NoAbsolutePathError(RESTException):
    """The given path is not absolute."""

    code = 400
    description = 'Please provide an absolute path ' \
        '(e.g. /bigdata/rz/test.jpg).'


class UnsupportedProtocolError(RESTException):
    """Given protocol is not supported."""

    code = 400
    allowed_protocols = ['https']
    description = 'Unsupported protocol. Use one of {0}'.format(
        str(allowed_protocols)
    )


class FileTooLargeError(RESTException):
    """Give file exceeds file size limits."""

    code = 400
    description = 'The given file is too large. The default limit for a ' \
        'dataset per record is {0} GiB. The default maximum file size is ' \
        '{1} GiB.'.format(
            100,
            50,
        )


class NoFileError(RESTException):
    """A valid regular file path must be given."""

    code = 400
    description = 'Please provide an absolute file path. Upload of ' \
        'directories is not supported.'


class FileDoesNotExist(RESTException):
    """Given file does not exist."""

    code = 400
    description = 'The given file does not exist or you do not have ' \
        'read permissions.'


class AuthenticationError(RESTException):
    """SSH Authentication failes."""

    code = 400
    description = 'SSH Authentication failed. Probably the SSH Key is ' \
        'not added to the remote server. Try to add it manually or ' \
        'contact the support.'


class SSHException(RESTException):
    """SSH connection cannot be established."""

    code = 400
    description = 'SSH connection cannot be established, currently. Please ' \
        'try again later.'


class NoPathError(RESTException):
    """No path is given."""

    code = 400
    description = 'Please provide a valid directory path.'


class RequestError(RESTException):
    """An error during an HTTP request."""

    code = 400
    description = 'An error occured while connecting to the specified URL. ' \
        'Please check and try again. Contact us, if the problem persists.'
