# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Module for invenio for server-side download from a given URL."""

from . import config
from .receivers import upload_failed_receiver, upload_finished_receiver
from .signals import upload_failed, upload_finished
from .utils import delete_ssh_key, deploy_ssh_key, make_object


class _UploadByURLState(object):
    """Uploady by URL state to store customized functions."""

    def __init__(self, app):
        """Initialize state."""
        self.app = app
        self.deploy_handler = {}
        self.delete_handler = {}

        for remote_server, conf in app.config[
                'UPLOADBYURL_REMOTE_SERVERS'].items():
            # add customized deploy functions
            self.deploy_handler[remote_server] = make_object(
                conf.get('deploy_handler', deploy_ssh_key))
            self.delete_handler[remote_server] = make_object(
                conf.get('delete_handler', delete_ssh_key))


class InvenioUploadByURL(object):
    """Invenio-UploadByURL extension."""

    def __init__(self, app=None, redisstore=None):
        """Extension initialization."""
        if app:
            self.init_app(app, redisstore=redisstore)

    def init_app(self, app, redisstore=None):
        """Flask application initialization."""
        self.init_config(app)

        # create redisstore if it does not exist
        if redisstore is None:
            import redis
            from simplekv.memory.redisstore import RedisStore

            self.redisstore = RedisStore(redis.StrictRedis.from_url(
                app.config['UPLOADBYURL_REDISSTORE_URL']
            ))
        else:
            self.redisstore = redisstore

        self.state = _UploadByURLState(app)
        app.extensions['invenio-uploadbyurl'] = self
        self.register_signals(app)

    def register_signals(self, app):
        """Register the signals."""
        upload_finished.connect(upload_finished_receiver)
        upload_failed.connect(upload_failed_receiver)

    def init_config(self, app):
        """Initialize configuration."""
        for k in dir(config):
            if k.startswith('UPLOADBYURL_'):
                app.config.setdefault(k, getattr(config, k))
