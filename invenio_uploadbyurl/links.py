# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Link factory to add module specific link."""

from flask import url_for
from invenio_records_files.api import Record

from .models import RemoteServer


def default_uploadbyurl_link_factory(pid):
    """Factory for uploadbyurl link generation."""
    try:
        record = Record.get_record(pid.get_assigned_object())
        bucket = record.files.bucket

        links = dict(
            uploadviaurl=url_for(
                'invenio_uploadbyurl.uploadbyurl_api',
                bucket_id=bucket.id,
                _external=True
            ),
        )

        return links
    except AttributeError:
        return None
