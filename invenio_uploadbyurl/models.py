# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Module for invenio for server-side download from a given URL."""

import re
from datetime import date

from flask import current_app
from invenio_accounts.models import User
from invenio_db import db
from sqlalchemy.orm import validates
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy_utils import EncryptedType

slug_pattern = re.compile('^[a-z][a-z0-9-]+$')


def _secret_key():
    """Return secret key from current application."""
    return current_app.config.get('SECRET_KEY')


class RemoteServer(db.Model):
    """Remote server information."""

    __tablename__ = 'uploadbyurl_remoteserver'

    #
    # Fields
    #

    id = db.Column(
        db.Integer,
        primary_key=True,
        autoincrement=True,
    )
    """Primary key."""

    name = db.Column(db.String(20), unique=True, nullable=False)
    """str: External identifier of the location."""

    access_protocol = db.Column(db.String(32), default='sftp', nullable=False)
    """str: Access protocol used to access the RemmoteServer."""

    server_address = db.Column(db.String(255), nullable=False)
    """str: Address of the RemoteServer."""

    port = db.Column(db.Integer, default=22, nullable=False)
    """int: SSH port of the remote server."""

    description = db.Column(
        db.Text, default='Empty description.', nullable=False)
    """str: Description of the remote server."""

    @validates('name')
    def validate_name(self, key, name):
        """
        Validate attribute name.

        Limits length of name to 20 characters and makes sure that the name
        only containers lower-case alphanumeric characters and dashes.

        Args:
            name(str): Name of the remote server to be validated.

        Returns:
            str: The name of the RemoteServer.

        Raises:
            ValueError: if name is longer than 20 characters or contains other
                characters than lower-case alphanumeric and dashes.

        """
        if not slug_pattern.match(name) or len(name) > 20:
            raise ValueError(
                'Invalid server name (lower-case alphanumeric + dashes).')
        return name

    @classmethod
    def get_by_name(cls, name):
        """
        Fetch a specific remote server object by its name.

        Args:
            name: The name of the RemoteServer which should be returned.

        Returns:
            RemoteServer: The RemoteServer object if found, otherwise
            ``None``.

        """
        return cls.query.filter_by(
            name=name,
        ).one_or_none()

    @classmethod
    def all(cls):
        """
        Return query that fetches all remote servers.

        Returns:
            list: List of all registered RemoteServer objects.

        """
        return RemoteServer.query.all()


class SSHKey(db.Model):
    """Storage for user-related SSH keys."""

    __tablename__ = 'uploadbyurl_sshkeys'

    #
    # Fields
    #

    id = db.Column(
        db.Integer,
        primary_key=True,
        autoincrement=True,
    )
    """int: Primary key."""

    private_key = db.Column(
        EncryptedType(type_in=db.Text, key=_secret_key), nullable=False
    )
    """str: SSH private key."""

    valid_until = db.Column(db.Date, default=date.min, nullable=False)
    """datetime.date: Expiration date of SSH key pair."""

    keytype = db.Column(db.String(10), default='rsa', nullable=False)
    """str: Type of ssh key."""

    username = db.Column(db.String(20), nullable=False)
    """str: Username used to connect to server."""

    user_id = db.Column(
        db.Integer,
        db.ForeignKey(User.id),
        nullable=False,
    )
    """int: Local user linked with a remote server."""

    remote_server_id = db.Column(
        db.Integer,
        db.ForeignKey(RemoteServer.id),
        nullable=False,
    )
    "int: ID of remote server to be linked with the user via SSHKey."

    # Relationship to user
    user = db.relationship(User, backref='ssh_key')
    """invenio_accounts.models.User: Backref to user."""

    remote_server = db.relationship(RemoteServer, backref='remote_server')
    """invenio_accounts.models.User: Backref to remote server."""

    # Unique constraint
    __table_args__ = (db.UniqueConstraint('user_id', 'remote_server_id'),)
    """RemoteServer: Only allow one key per user and remote_server."""

    @classmethod
    def create(cls, private_key, username, user, remote_server,
               valid_until=None, keytype='rsa'):
        """
        Create the SSH key pair.

        Args:
            private_key(str): The private key string to be stored in the db
                object.
            username(str): The username of the user on the remote server.
            user(invenio_accounts.models.User): The user object to be linked
                with the SSHKey.
            remote_server(RemoteServer): The remote
                server object the SSH Key should be linked with.
            valid_until(datetime.date): Date until the SSH Key should be
                valid. Not used yet.
            keytype(str): The type of the key which should be added. The
                available types are stored in the configuration variable
                ``invenio_uploadbyurl.config.UPLOADBYURL_KEY_DISPATCH_TABLE``.

        Returns:
            SSHKey: The SSH Key object.

        """
        # Check if key already exists
        with db.session.begin_nested():
            if not valid_until:
                valid_until = date.min
            obj = cls(
                private_key=private_key,
                valid_until=valid_until,
                keytype=keytype,
                username=username,
                user=user,
                remote_server=remote_server,
            )
            db.session.add(obj)
        return obj

    @classmethod
    def delete(cls, user_id, remote_server_id):
        """
        Delete SSHKey entry from database.

        Args:
            user_id(int): The id of the user the key belongs to.
            remote_server_id(int): The id of the RemoteServer the SSHKey is
                associated with.

        """
        with db.session.begin_nested():
            obj = cls.query.filter_by(
                user_id=user_id,
                remote_server_id=remote_server_id
            ).one()
            db.session.delete(obj)

    @classmethod
    def get(cls, user_id, remote_server_id):
        """
        Retrieve ssh key pair.

        Args:
            user_id(int): The id of the user the key belongs to.
            remote_server_id(int): The id of the RemoteServer the SSHKey is
                associated with.

        """
        key = cls.query.filter_by(
            user_id=user_id,
            remote_server_id=remote_server_id).one_or_none()
        return key

    @classmethod
    def update_key(cls, user_id, remote_server_id,
                   private_key, valid_until=None):
        """
        Update private key string.

        Args:
            user_id(int): The id of the user the key belongs to.
            remote_server_id(int): The id of the RemoteServer the SSHKey is
                associated with.
            private_key(str): The private key string to be stored in the db
                object.
            valid_until(datetime.date): Date until the SSH Key should be
                valid. Not used yet.

        """
        key = cls.get(user_id, remote_server_id)
        if not valid_until:
            valid_until = date.min
        with db.session.begin_nested():
            key.private_key = private_key
            key.valid_until = valid_until
            db.session.add(key)
        return key
