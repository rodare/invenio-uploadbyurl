# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Uploadbyurl module receivers."""

from flask import current_app

from .utils import notification_mail


def upload_finished_receiver(sender, user_id=None, **kwargs):
    """Send email after upload is finished."""
    if current_app.config['UPLOADBYURL_NOTIFICATION_ENABLED']:
        notification_mail(user_id)


def upload_failed_receiver(sender, user_id=None,
                           filepath=None, url=None, **kwargs):
    """Send email after upload is finished."""
    if current_app.config['UPLOADBYURL_NOTIFICATION_ENABLED']:
        notification_mail(user_id, failed=True, filepath=filepath, url=url)
