# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Celery tasks for file upload."""

import os
import stat
import subprocess
import tempfile
from io import StringIO
from urllib.parse import urlparse

import paramiko
import pexpect
import requests
from celery import shared_task
from flask import current_app
from invenio_db import db
from invenio_files_rest.errors import FileSizeError
from invenio_files_rest.models import Bucket, FileInstance, ObjectVersion
from werkzeug.local import LocalProxy

from .models import SSHKey
from .signals import upload_failed, upload_finished
from .utils import delete_all_keys, delete_old_jobs

_redisstore = LocalProxy(
    lambda: current_app.extensions['invenio-uploadbyurl'].redisstore)


def create_objectversion_from_url(bucket_id, url, key=None):
    """Download file from given URL and create new ObjectVersion."""
    # HEAD request to try to make remote compute Content-Length
    resp = requests.head(url, allow_redirects=True)
    # Download the file
    resp = requests.get(url, stream=True, allow_redirects=True)

    # Check response status and raise error in case of failure
    resp.raise_for_status()

    # retrieve size
    size = int(resp.headers.get('Content-Length', 0))

    # get key from url using urllib
    url_parts = urlparse(url)
    if not key:
        key = url_parts[2].rpartition('/')[2] or 'data'

    ObjectVersion.create(
        bucket=bucket_id,
        key=key,
        stream=resp.raw,
        size=size or None,
        mimetype=resp.headers.get('Content-Type'),
    )
    db.session.commit()


@shared_task(max_retries=6, default_retry_delay=10 * 60, rate_limit='100/m')
def download_files(bucket_id, user_id, url_list, key=None):
    """Split given list and create downloading tasks."""
    try:
        for url in url_list:
            create_objectversion_from_url(bucket_id, url, key)
        upload_finished.send(
            current_app._get_current_object(), user_id=user_id)
    except Exception as exc:
        download_files.retry(exc=exc)
        if download_files.retries == download_files.max_retries:
            upload_failed.send(
                current_app._get_current_object(), user_id=user_id,
                url=url
            )


@shared_task(max_retries=6, default_retry_delay=10 * 60, rate_limit='100/m')
def download_via_sftp(bucket_id, remote_server_id, user_id, filepath):
    """
    Download a file via SFTP protocol in a celery task.

    The file transfer is performed via the SFTP terminal program. Paramiko
    integrated file transfer is way slower. The SSH private key is stored as
    an encrypted temporary file on disk. The encryption key is generated
    within this celery task, and only available as long as the file transfer
    is active.

    Args:
        bucket_id(str): ID of the bucket to be used to store the file.
        remote_server_id(int): the ID of the RemoteServer.
        user_id(int): The ID of the user.
        filepath(str): The path of the regular file which should be
            downloaded.
    Returns:
        None

    Raises:
        IOError - if bucket does not exist or if filepath does not specify a
            regular file.

    """
    try:
        key = SSHKey.get(user_id, remote_server_id)
        pkey = current_app.config[
            'UPLOADBYURL_KEY_DISPATCH_TABLE'][key.keytype].from_private_key(
                StringIO(key.private_key))
        bucket = Bucket.get(bucket_id)
        if not bucket:
            raise IOError()
        location = bucket.location
        # get filesize and check if is file via paramiko
        with paramiko.SSHClient() as client:
            client.load_system_host_keys()
            client.connect(key.remote_server.server_address,
                           username=key.username,
                           pkey=pkey)
            sftp = client.open_sftp()
            try:
                file_stat = sftp.stat(filepath)
                if stat.S_ISREG(file_stat.st_mode):
                    size = sftp.stat(filepath).st_size
                # check file size limits
                if (bucket.quota_left < size or
                        bucket.size_limit < size):
                    raise FileSizeError()
            except IOError:
                current_app.logger.error('File does not exist.')
                raise IOError()
        with tempfile.NamedTemporaryFile() as key_file:
            # set correct restrictive permissions
            os.chmod(key_file.name, stat.S_IRUSR | stat.S_IWUSR)
            secret = os.urandom(24).hex()
            pkey.write_private_key_file(key_file.name, password=secret)
            # create invenio-files-rest stuff
            filename = os.path.basename(filepath)
            file_instance = FileInstance.create()
            file_instance.init_contents(
                default_location=location.uri,
                default_storage_class=current_app.config[
                    'FILES_REST_DEFAULT_STORAGE_CLASS'])
            # call sftp via subprocess
            arguments = '-i {key} -P {port} {user}@{server}'.format(
                key=key_file.name,
                port=key.remote_server.port,
                user=key.username,
                server=key.remote_server.server_address,
            )
            cmd_get = "get {remote_path} {local_path}".format(
                remote_path=filepath,
                local_path=file_instance.uri,
            )
            try:
                sftp_proc = pexpect.spawn(
                    '/usr/bin/sftp', arguments.split(),
                    timeout=current_app.config['UPLOADBYURL_TIMEOUT']
                )
                sftp_proc.expect(
                    'Enter passphrase for key .*',
                    timeout=current_app.config['UPLOADBYURL_SHORT_TIMEOUT'])
                sftp_proc.sendline(secret)
                sftp_proc.expect(
                    'sftp>',
                    timeout=current_app.config['UPLOADBYURL_SHORT_TIMEOUT'])
                # download file with timeout specified in configuration
                sftp_proc.sendline(cmd_get)
                sftp_proc.expect(
                    'sftp>', timeout=current_app.config['UPLOADBYURL_TIMEOUT']
                )
                sftp_proc.sendline('exit')
                sftp_proc.expect(
                    pexpect.EOF,
                    timeout=current_app.config['UPLOADBYURL_SHORT_TIMEOUT'])
                sftp_proc.close()
            except (pexpect.EOF, pexpect.TIMEOUT) as e:
                # clean up in case of failure
                file_instance.storage().delete()
                file_instance.delete()
                sftp_proc.terminate()
                raise e
        # make fileinstance ready to be readable
        file_instance.set_uri(
            uri=file_instance.uri,
            size=size,
            checksum='',
        )
        # compute checksum
        file_instance.update_checksum()
        db.session.add(file_instance)
        object_version = ObjectVersion.create(
            bucket=bucket_id,
            key=filename,
            _file_id=file_instance,
        )
        db.session.add(object_version)
        db.session.commit()
        upload_finished.send(
            current_app._get_current_object(), user_id=user_id)
    except Exception as exc:
        download_via_sftp.retry(exc=exc)
        if download_via_sftp.retries == download_via_sftp.max_retries:
            upload_failed.send(
                current_app._get_current_object(), user_id=user_id,
                filepath=filepath)


@shared_task(ignore_result=True)
def clear_old_jobs():
    """Delete old cache entries from Redis."""
    try:
        delete_old_jobs()
    except Exception as exc:
        clear_old_jobs.retry(exc=exc)


@shared_task(ignore_result=True)
def clear_redisstore():
    """Clear all key value pairs in redisstore."""
    try:
        delete_all_keys()
    except Exception as exc:
        clear_redisstore.retry(exc=exc)
