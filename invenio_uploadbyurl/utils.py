# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Upload by URL Rest API."""

import json
from datetime import date, datetime, timedelta
from io import StringIO

import celery.states
import paramiko
import six
from celery.result import AsyncResult
from flask import current_app, flash
from flask_login import current_user
from invenio_accounts.models import User
from invenio_db import db
from invenio_db.utils import rebuild_encrypted_properties
from werkzeug.local import LocalProxy
from werkzeug.utils import import_string

from .models import RemoteServer, SSHKey
from .proxies import current_uploadbyurl

_redisstore = LocalProxy(
    lambda: current_app.extensions['invenio-uploadbyurl'].redisstore)


def create_cache_key(user_id):
    """Transform user_id to redis key by adding prefix."""
    return '{0}#celery_id#{1}'.format(
        current_app.config['UPLOADBYURL_KV_PREFIX'],
        str(user_id),
    )


def create_cache_entry(celery_id, bucket_id, location, protocol):
    """Create the cache entry storing relevant information."""
    return dict(
        bucket_id=str(bucket_id),
        celery_id=str(celery_id),
        created=datetime.now().timestamp(),
        location=location,
        protocol=protocol,
    )


def append_cache_entry(key, entry):
    """Create cache key or append the entry to existing key."""
    try:
        data = _redisstore.get(key).decode('utf-8')
        job_list = json.loads(data)
        job_list.append(entry)
    except KeyError:
        job_list = [entry]
    data = json.dumps(job_list).encode('utf-8')
    _redisstore.put(
        key, data,
        ttl_secs=current_app.config['UPLOADBYURL_REDIS_KEY_TTL']
    )


def limit(iterator, limit=500):
    """Generator to limit number of iterations."""
    n = 0
    while n < limit:
        yield iterator.__next__()
        n += 1


def delete_all_keys():
    """Delete all keys from kv-store."""
    for key in _redisstore.iter_keys():
        if key.startswith(current_app.config['UPLOADBYURL_KV_PREFIX']):
            _redisstore.delete(key)


def delete_old_jobs():
    """Delete job information older than x days from kv-store."""
    for key in _redisstore.iter_keys():
        if key.startswith(current_app.config['UPLOADBYURL_KV_PREFIX']):
            data = _redisstore.get(key)
            job_list = json.loads(data.decode('utf-8'))
            job_list = [x for x in job_list if not is_older_than(x)]
            if job_list:
                _redisstore.put(
                    key, json.dumps(job_list).encode('utf-8'),
                    ttl_secs=current_app.config['UPLOADBYURL_REDIS_KEY_TTL'])
            else:
                _redisstore.delete(key)


def is_older_than(entry):
    """Return True if timestamp of entry is older than x days."""
    cache_duration = current_app.config['UPLOADBYURL_CACHE_DURATION']
    created = entry['created']
    delta = datetime.now() - datetime.fromtimestamp(created)
    if delta > timedelta(days=cache_duration):
        return True
    return False


def deploy_ssh_key(pub_key, server, username, password, prv):
    """
    Deploy SSH key to remote machine.

    This is the default function. It can be overwritten via the configuration
    variable ``UPLOADBYURL_REMOTE_SERVERS``.
    """
    with paramiko.SSHClient() as client:
        client.load_system_host_keys()
        client.connect(server, username=username, password=password)
        # Create .ssh directory in case it does not exist
        stdin, stdout, stderr = client.exec_command('mkdir -p ~/.ssh')
        status = stdout.channel.recv_exit_status()
        if status:
            flash('Could not create .ssh directory. Copy the key manually.')
            return
        stdin, stdout, stderr = client.exec_command(
            'echo "%s" >> ~/.ssh/authorized_keys' % pub_key)
        status = stdout.channel.recv_exit_status()
        if status:
            flash('Could not add the SSH public key to authorized_keys. '
                  'Add it manually.')
            return
        # set correct permissions
        stdin, stdout, stderr = client.exec_command(
            'chmod 600 ~/.ssh/authorized_keys')
        status = stdout.channel.recv_exit_status()
        if status:
            flash('Could not set correct access rights '
                  'for ~/.ssh/authorized_keys.')
            return
        stdin, stdout, stderr = client.exec_command('chmod 700 ~/.ssh/')
        status = stdout.channel.recv_exit_status()
        if status:
            flash('Could not set correct access '
                  'rights for directory ~/.ssh/.')
            return


def delete_ssh_key(remote):
    """Delete SSH key from remote machine."""
    # retrieve SSH key
    key = SSHKey.get(current_user.id, remote.id)
    if not key:
        flash('Nothing deleted, connection was not setup before.',
              category='warning')
        return
    pkey = current_app.config[
        'UPLOADBYURL_KEY_DISPATCH_TABLE'][key.keytype].from_private_key(
            StringIO(key.private_key))
    with paramiko.SSHClient() as client:
        try:
            client.load_system_host_keys()
            client.connect(remote.server_address,
                           username=key.username, pkey=pkey)
            command = 'sed -i \'/{pattern}/d\' ~/.ssh/authorized_keys'.format(
                pattern=current_app.config['UPLOADBYURL_COMMENT']
            )
            stdin, stdout, stderr = client.exec_command(command)
            if stdout.channel.recv_exit_status() != 0:
                flash('Something went wrong on our side. Please check '
                      'manually if the public key was removed successfully.',
                      category='warning')
        except paramiko.ssh_exception.AuthenticationException:
            flash('The public key seems to be already deleted from the '
                  'remote server. Deleting from database only.',
                  category='alert')
        except Exception:
            flash('Something went wrong on our side. Please check '
                  'manually if the public key was removed successfully.',
                  category='warning')
        finally:
            client.close()
    # delete key from db
    SSHKey.delete(user_id=current_user.id, remote_server_id=remote.id)
    db.session.commit()


def generate_public_keystr(key):
    """
    Generate public key string from private key.

    Arguments:
        key(str): The private key string.

    Returns:
        str: The corresponding public key.

    """
    pkey = current_app.config['UPLOADBYURL_KEY_DISPATCH_TABLE'][
        key.keytype].from_private_key(StringIO(key.private_key))
    pub_key = '{type} {key} {comment}'.format(
        type=pkey.get_name(),
        key=pkey.get_base64(),
        comment=current_app.config['UPLOADBYURL_COMMENT'],
    )
    return pub_key


def generate_rsa_key():
    """
    Generate an SSH RSA key.

    Returns:
        tuple: 2-element tuple containing:

            - (str): The generated RSA private key string.
            - (str): The corresponding publick key string.

    """
    key = paramiko.RSAKey.generate(bits=4096)
    key_str = StringIO()
    key.write_private_key(key_str)
    pub_key = '{type} {key} {comment}'.format(
        type=key.get_name(),
        key=key.get_base64(),
        comment=current_app.config['UPLOADBYURL_COMMENT'],
    )
    return key_str.getvalue(), pub_key


def connect_user_and_server(remote, username, password):
    """Function to generate SSH key to transfer to remote server."""
    prv, pub = generate_rsa_key()
    # deploy SSH key to server
    try:
        current_uploadbyurl.deploy_handler.get(remote.name, deploy_ssh_key)(
            pub, remote.server_address, username, password, prv)
        key = SSHKey.create(private_key=prv, username=username,
                            user=current_user, remote_server=remote,
                            keytype='rsa')
        db.session.commit()
    except paramiko.ssh_exception.AuthenticationException:
        flash('Bad credentials. Please try again.', category='danger')
        return
    except Exception:
        flash('Something went wrong. Please try again.', category='danger')
        return


def notification_mail(user_id, failed=False, filepath=None, url=None):
    """Send notification mail after finished upload."""
    if user_id in current_app.config['UPLOADBYURL_USER_NOTIFICATIONS_OFF']:
        return None

    from flask_mail import Message
    from invenio_mail.tasks import send_email

    if failed:
        if filepath:
            msg_body = current_app.config[
                'UPLOADBYURL_EMAIL_BODY_FAILED'].format(filepath=filepath)
        elif url:
            msg_body = current_app.config[
                'UPLOADBYURL_EMAIL_BODY_FAILED_URL'].format(url=url)
        msg_title = current_app.config['UPLOADBYURL_EMAIL_TITLE_FAILED']
    else:
        msg_title = current_app.config['UPLOADBYURL_EMAIL_TITLE']
        msg_body = current_app.config['UPLOADBYURL_EMAIL_BODY']

    sender = current_app.config['UPLOADBYURL_SENDER_EMAIL']

    email = User.query.filter_by(id=user_id).one().email

    msg = Message(
        msg_title,
        sender=sender,
        recipients=[email, ],
        body=msg_body,
    )

    send_email.delay(msg.__dict__)


def make_object(value, default=None):
    """Make a handler to configure specific functions."""
    if isinstance(value, six.string_types):
        return import_string(value)
    else:
        return value
    return default


def rebuild_private_keys(old_key):
    """
    Rebuild the private keys when SECRET_KEY is changed.

    Args:
        old_key(str): The old SECRET_KEY value.

    """
    current_app.logger.info('Rebuilding SSHKey.private_key...')
    rebuild_encrypted_properties(old_key, SSHKey, ['private_key'])


def is_upload_active(user_id, bucket_id):
    """
    Check if a background upload job is active.

    Args:
        user_id(int): User id to check the upload status for.
        bucket_id(uuid.UUID): The bucket that shall be checked for active jobs.

    Returns:
        bool: True, if a background job is active or pending for the bucket
              False, if no background job is active for the bucket.

    """
    try:
        data = _redisstore.get(create_cache_key(user_id))
    except KeyError:
        return False
    job_list = json.loads(data.decode('utf-8'))
    for job in job_list:
        if str(bucket_id) != job['bucket_id']:
            continue
        res = AsyncResult(job['celery_id'])
        if res.state in celery.states.UNREADY_STATES:
            return True
    return False
