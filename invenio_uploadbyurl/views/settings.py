# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Account settings blueprint for invenio-uploadbyurl."""

import json
from datetime import datetime

import celery.states
from celery.result import AsyncResult
from flask import Blueprint, abort, current_app, redirect, render_template, \
    request, url_for
from flask_babelex import gettext as _
from flask_breadcrumbs import register_breadcrumb
from flask_login import current_user, login_required
from flask_menu import register_menu
from werkzeug.local import LocalProxy

from ..errors import RemoteServerNotFoundError
from ..forms import ConnectForm
from ..models import RemoteServer, SSHKey
from ..proxies import current_uploadbyurl
from ..utils import connect_user_and_server, create_cache_key, \
    delete_ssh_key, generate_public_keystr

_redisstore = LocalProxy(
    lambda: current_app.extensions['invenio-uploadbyurl'].redisstore)

blueprint = Blueprint(
    'invenio_uploadbyurl_settings',
    __name__,
    url_prefix='/account/settings/remoteserver',
    static_folder='../static',
    template_folder='../templates',
)


@blueprint.record_once
def post_ext_init(state):
    """Set necessary config."""
    app = state.app

    app.config.setdefault(
        "UPLOADBYURL_SETTINGS_TEMPLATE",
        app.config.get("SETTINGS_TEMPLATE",
                       "invenio_uploadbyurl/settings/base.html"))


@blueprint.route('/', methods=['GET', 'POST'])
@login_required
@register_menu(
    blueprint, 'settings.uploadbyurl',
    _('%(icon)s Remote Server', icon='<i class="fa fa-server fa-fw"></i>'),
    order=15,
    active_when=lambda: request.endpoint.startswith(
        'invenio_uploadbyurl_settings'),
    visible_when=lambda: bool(current_app.config.get(
        'UPLOADBYURL_ACTIVATE')) is not False
)
@register_breadcrumb(
    blueprint, 'breadcrumbs.settings.uploadbyurl', _('Remote Server')
)
def index():
    """List available remote servers."""
    remote_servers = RemoteServer.all()

    # check if user is already connected
    for remote in remote_servers:
        key = SSHKey.get(current_user.id, remote.id)
        remote.connected = True if key else False
        remote.pub_key = generate_public_keystr(key) if key else None

    # Find recent background jobs
    try:
        key = create_cache_key(current_user.id)
        data = _redisstore.get(key)
        job_list = json.loads(data.decode('utf-8'))
    except KeyError:
        job_list = []

    # sort job_list by date
    job_list.sort(key=lambda x: x['created'], reverse=True)

    for job in job_list:
        res = AsyncResult(job['celery_id'])
        if res.state in celery.states.PROPAGATE_STATES:
            job['state'] = 'FAILURE'
        elif res.state == celery.states.STARTED:
            job['state'] = 'STARTED'
        elif res.state in celery.states.UNREADY_STATES:
            job['state'] = 'PENDING'
        else:
            job['state'] = 'SUCCESS'

        job['created'] = datetime.fromtimestamp(job['created'])

    return render_template(
        'invenio_uploadbyurl/settings/index.html',
        remote_servers=remote_servers,
        formclass=ConnectForm,
        job_list=job_list,
    )


@blueprint.route('/init/<remote_name>', methods=['GET', 'POST'])
@login_required
def init(remote_name):
    """Initialiaze connection to remote server."""
    remote = RemoteServer.get_by_name(remote_name)
    if not remote:
        raise RemoteServerNotFoundError()
    connect_form = ConnectForm()

    if connect_form.validate_on_submit():
        username = connect_form.username.data
        password = connect_form.password.data
        connect_user_and_server(remote, username, password)
        return redirect(url_for('invenio_uploadbyurl_settings.index'))

    return render_template(
        'invenio_uploadbyurl/settings/init.html',
        remote=remote,
        connect_form=connect_form,
    )


@blueprint.route('/delete/<remote_name>', methods=['GET', 'POST'])
@login_required
def delete(remote_name):
    """Delete connection with remote server for current user."""
    remote = RemoteServer.get_by_name(remote_name)
    if not remote:
        raise RemoteServerNotFoundError()
    current_uploadbyurl.delete_handler.get(remote.name, delete_ssh_key)(remote)
    return redirect(url_for('invenio_uploadbyurl_settings.index'))
