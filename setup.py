# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Module for invenio that integrates server-side download from a given URL in asynchronous tasks."""

import os

from setuptools import find_packages, setup

readme = open('README.rst').read()
history = open('CHANGES.rst').read()

tests_require = [
    'blinker>=1.4',
    'check-manifest>=0.25',
    'coverage>=4.0',
    'httpretty>=1.0.3',
    'invenio-access>=1.0.0,<1.3',
    'invenio-celery>=1.0.0,<1.2.3',
    'invenio-i18n>=1.0.0',
    'invenio-theme>=1.0.0',
    'invenio-mail>=1.0.0',
    'invenio-search-ui>=1.0.0',
    'isort>=4.3.3',
    'pydocstyle>=1.0.0',
    'pytest-cache>=1.0',
    'pytest-cov>=1.8.0',
    'pytest-pep8>=1.0.6',
    'pytest>=3.1.0,<6',
]

invenio_db_version = '>=1.0.1'
invenio_deposit_version = '==1.0.0a11'
flask_sqlalchemy_version = '==2.3.2'
sqlalchemy_version = '==1.2.19'

extras_require = {
    'docs': [
        'Sphinx>=2.0',  # FIXME: Remove pinning when
                               # sphinx-contrib-versioning is compatible.
        'sphinx_rtd_theme>=0.2.4',
        'docutils>=0.12,<0.18',
    ],
    'mysql': [
        'invenio-db[mysql,versioning]{}'.format(invenio_db_version),
        'Flask-SQLAlchemy{}'.format(flask_sqlalchemy_version),
        'SQLAlchemy{}'.format(sqlalchemy_version),
    ],
    'postgresql': [
        'invenio-db[postgresql,versioning]{}'.format(invenio_db_version),
        'Flask-SQLAlchemy{}'.format(flask_sqlalchemy_version),
        'SQLAlchemy{}'.format(sqlalchemy_version),
    ],
    'sqlite': [
        'invenio-db[versioning]{}'.format(invenio_db_version),
        'Flask-SQLAlchemy{}'.format(flask_sqlalchemy_version),
        'SQLAlchemy{}'.format(sqlalchemy_version),
    ],
    'elasticsearch2': [
        'invenio-deposit[elasticsearch2]{}'.format(invenio_deposit_version),
    ],
    'elasticsearch5': [
        'invenio-deposit[elasticsearch5]{}'.format(invenio_deposit_version),
    ],
    'elasticsearch6': [
        'invenio-deposit[elasticsearch6]{}'.format(invenio_deposit_version),
    ],
    'elasticsearch7': [
        'invenio-deposit[elasticsearch7]{}'.format(invenio_deposit_version),
    ],
    'tests': tests_require,
}

extras_require['all'] = []
for name, reqs in extras_require.items():
    if name in ('mysql', 'postgresql', 'sqlite', 'elasticsearch2', 'elasticsearch5', 'elasticsearch6', 'elasticsearch7'):
        continue
    extras_require['all'].extend(reqs)

setup_requires = [
    'Babel>=1.3',
    'pytest-runner>=2.6.2',
]

install_requires = [
    'celery>=4.0.2,<4.4.3',
    'Flask==1.1.4',
    'Flask-OAuthlib>=0.9.3',
    'Flask-WTF>=0.14.2,<0.15.0',
    'Flask-Login>=0.4.1,<0.5.0',
    'humanize>=0.5.1',
    'invenio-access>=1.1.0,<1.2.0',
    'invenio-accounts>=1.0.2,<1.4',
    'invenio-admin>=1.0.0,<1.3',
    'invenio-assets>=1.0.0,<1.2.0',
    'invenio-files-rest==1.0.0a23.post3',
    'invenio-i18n>=1.0.0',
    'invenio-indexer>=1.1.0,<1.2.0',
    'invenio-records-files>=1.0.0a10',
    'invenio-records-rest>=1.6.2',
    'invenio-rest>=1.1.3',
    'invenio-jsonschemas>=1.0.0,<1.1.0',
    'invenio-cache>=1.0.0',
    'oauthlib>=1.1.2,!=2.0.0,!=2.0.3,!=2.0.4,!=2.0.5,<3.0.0',
    'paramiko>=2.9.0',
    'pexpect>=4.0.1',
    'redis>=2.10.5',
    'requests-oauthlib>=0.5.0,<1.2.0',
    'werkzeug>=0.15.0,<1.0',
    # "requests" has hard version range dependency on "idna" and "urllib3"
    # Every time "idna" and "urllib3" are updated, installation breaks because
    # "requests" dependencies are not resolved properly.
    'urllib3<1.25,>=1.21.1',  # from "requests"
    'idna>=2.5,<2.8',  # from "requests"
]

packages = find_packages()


# Get the version string. Cannot be done with import!
g = {}
with open(os.path.join('invenio_uploadbyurl', 'version.py'), 'rt') as fp:
    exec(fp.read(), g)
    version = g['__version__']

setup(
    name='invenio-uploadbyurl',
    version=version,
    description=__doc__,
    long_description=readme + '\n\n' + history,
    keywords='invenio background-upload',
    license='GPLv3',
    author='HZDR',
    author_email='t.frust@hzdr.de',
    url='https://github.com/frust45/invenio-uploadbyurl',
    packages=packages,
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    entry_points={
        'invenio_admin.views': [
            'remoteserver_adminview = '
            'invenio_uploadbyurl.admin:remoteserver_adminview',
            'sshkey_adminview = '
            'invenio_uploadbyurl.admin:sshkey_adminview',
        ],
        'invenio_base.apps': [
            'invenio_uploadbyurl = invenio_uploadbyurl:InvenioUploadByURL',
        ],
        'invenio_base.api_apps': [
            'invenio_uploadbyurl = invenio_uploadbyurl:InvenioUploadByURL',
        ],
        'invenio_base.api_blueprints': [
            'invenio_uploadbyurl = invenio_uploadbyurl.api:blueprint',
        ],
        'invenio_base.blueprints': [
            'invenio_uploadbyurl_settings = '
            'invenio_uploadbyurl.views.settings:blueprint',
        ],
        'invenio_assets.bundles': [
            'invenio_uploadbyurl_js = invenio_uploadbyurl.bundles:js',
            'invenio_uploadbyurl_dependencies_js = invenio_uploadbyurl.'
            'bundles:js_dependencies',
        ],
        'invenio_i18n.translations': [
            'messages = invenio_uploadbyurl',
        ],
        'invenio_celery.tasks': [
            'invenio_uploadbyurl = invenio_uploadbyurl.tasks',
        ],
        'invenio_db.alembic': [
            'invenio_uploadbyurl = invenio_uploadbyurl:alembic'
        ],
        'invenio_db.models': [
            'invenio_uploadbyurl = invenio_uploadbyurl.models',
        ],
    },
    extras_require=extras_require,
    install_requires=install_requires,
    setup_requires=setup_requires,
    tests_require=tests_require,
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Development Status :: 4 - Beta',
    ],
)
