# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Pytest configuration."""

import json
import os
import shutil
import tempfile
import uuid
from datetime import datetime, timedelta
from io import StringIO

import paramiko
import pytest
from flask import Flask
from flask_celeryext import FlaskCeleryExt
from flask_menu import Menu
from invenio_access import InvenioAccess
from invenio_access.models import ActionUsers
from invenio_accounts import InvenioAccounts
from invenio_accounts.testutils import create_test_user
from invenio_accounts.views import blueprint as accounts_blueprint
from invenio_assets import InvenioAssets
from invenio_cache import InvenioCache
from invenio_db import InvenioDB
from invenio_db import db as db_
from invenio_deposit import InvenioDeposit
from invenio_files_rest import InvenioFilesREST
from invenio_files_rest.models import Bucket, Location
from invenio_files_rest.permissions import bucket_read_all, \
    bucket_update_all, multipart_read_all, object_delete_all, \
    object_read_all
from invenio_files_rest.views import blueprint as files_rest_blueprint
from invenio_i18n import InvenioI18N
from invenio_mail import InvenioMail
from invenio_records import InvenioRecords
from sqlalchemy_utils.functions import create_database, database_exists, \
    drop_database

from invenio_uploadbyurl import InvenioUploadByURL
from invenio_uploadbyurl.api import blueprint
from invenio_uploadbyurl.models import RemoteServer, SSHKey
from invenio_uploadbyurl.utils import delete_all_keys
from invenio_uploadbyurl.views.settings import blueprint as settings_blueprint


@pytest.fixture()
def instance_path():
    """Temporary instance path."""
    path = tempfile.mkdtemp()
    yield path
    shutil.rmtree(path)


@pytest.fixture()
def base_app(instance_path):
    """Flask application fixture."""
    app_ = Flask('testapp', instance_path=instance_path)
    app_.config.update(
        ACCOUNTS_SESSION_REDIS_URL=os.getenv('ACCOUNTS_SESSION_REDIS_URL',
                                             'redis://localhost:6379/0'),
        CELERY_TASK_ALWAYS_EAGER=True,
        CELERY_RESULT_BACKEND="cache",
        CELERY_CACHE_BACKEND="memory",
        CELERY_TASK_EAGER_PROPAGATES=True,
        SECRET_KEY='SECRET_KEY',
        SQLALCHEMY_TRACK_MODIFICATIONS=False,
        SQLALCHEMY_DATABASE_URI=os.getenv('SQLALCHEMY_DATABASE_URI',
                                          'sqlite://'),
        SERVER_NAME='example.com',
        TESTING=True,
        UPLOADBYURL_NOTIFICATION_ENABLED=False,
        UPLOADBYURL_REMOTE_SERVERS={
            'foo': {
                'deploy_handler': 'invenio_uploadbyurl.utils:deploy_ssh_key',
                'delete_handler': 'invenio_uploadbyurl.utils:delete_ssh_key',
            }
        },
        UPLOADBYURL_REDISSTORE_URL=os.getenv('UPLOADBYURL_REDISSTORE_URL',
                                             'redis://localhost:6379/0'),
        UPLOADBYURL_TIMEOUT=30,
        UPLOADBYURL_SHORT_TIMEOUT=10,
        FILES_REST_DEFAULT_QUOTA_SIZE=100 * 1024 * 1024,
        MAIL_SUPPRESS_SEND=True,
        WTF_CSRF_ENABLED=False,
    )
    FlaskCeleryExt(app_)
    InvenioDB(app_)
    InvenioRecords(app_)
    InvenioAccounts(app_)
    InvenioAccess(app_)
    InvenioAssets(app_)
    InvenioCache(app_)
    InvenioFilesREST(app_)
    InvenioDeposit(app_)
    InvenioMail(app_)
    InvenioI18N(app_)
    app_.register_blueprint(accounts_blueprint)
    app_.register_blueprint(files_rest_blueprint)

    Menu(app_)

    return app_


@pytest.fixture()
def app(base_app):
    """Flask application fixture."""
    InvenioUploadByURL(base_app)
    base_app.register_blueprint(blueprint)
    base_app.register_blueprint(settings_blueprint)
    with base_app.app_context():
        yield base_app


@pytest.fixture()
def client(app):
    """Get test client."""
    with app.test_client() as test_client:
        yield test_client


@pytest.fixture()
def db(app):
    """Get setup database."""
    if not database_exists(str(db_.engine.url)):
        create_database(str(db_.engine.url))
    db_.create_all()
    yield db_
    db_.session.remove()
    db_.drop_all()


@pytest.fixture()
def dummy_location(db):
    """File system location."""
    tmppath = tempfile.mkdtemp()

    loc = Location(
        name='testloc',
        uri=tmppath,
        default=True
    )
    db.session.add(loc)
    db.session.commit()

    yield loc

    shutil.rmtree(tmppath)


@pytest.fixture()
def bucket(db, dummy_location):
    """Bucket object."""
    b1 = Bucket.create()
    db.session.commit()
    return b1


@pytest.fixture()
def user(db, bucket):
    """Create permissions."""
    user = create_test_user(
        email='bucket@hzdr.de',
        password='pass1',
        active=True,
    )

    bucket_perms = [
        bucket_read_all,
        object_read_all,
        bucket_update_all,
        object_delete_all,
        multipart_read_all,
    ]

    for perm in bucket_perms:
        db.session.add(ActionUsers(
            action=perm.value,
            argument=str(bucket.id),
            user=user))

    yield user
    # make sure cache is empty after test
    delete_all_keys()


@pytest.fixture()
def user2(db):
    """Create permissions."""
    user1 = create_test_user(
        email='user1@hzdr.de',
        password='pass1',
        active=True,
    )
    user_2 = create_test_user(
        email='user2@hzdr.de',
        password='pass1',
        active=True,
    )

    yield (user1, user_2)
    # make sure cache is empty after test
    delete_all_keys()


@pytest.fixture()
def remote(db, user, user2):
    """Read public and private key and store in db."""
    # add rsa key
    key_rsa = paramiko.RSAKey.from_private_key_file('tests/sftp/test-rsa')
    key_str_rsa = StringIO()
    key_rsa.write_private_key(key_str_rsa)
    # add ecdsa key
    key_ecdsa = paramiko.ECDSAKey.from_private_key_file(
        'tests/sftp/test-ecdsa')
    key_str_ecdsa = StringIO()
    key_ecdsa.write_private_key(key_str_ecdsa)
    server_name = os.getenv('SFTP_SERVER')
    with db.session.begin_nested():
        remote_server = RemoteServer(
            name='foo',
            access_protocol='sftp',
            server_address=server_name)
        db.session.add(remote_server)

    db.session.commit()

    SSHKey.create(private_key=key_str_rsa.getvalue(),
                  username='foo', user=user, remote_server=remote_server)
    SSHKey.create(private_key=key_str_ecdsa.getvalue(), username='foo',
                  user=user2[0], remote_server=remote_server, keytype='ecdsa')

    db.session.commit()

    yield remote_server


@pytest.fixture()
def sshkeys():
    """Create SSH key pair."""
    key = paramiko.RSAKey.generate(bits=4096)
    out1 = StringIO()
    key.write_private_key(out1)
    key = paramiko.RSAKey.generate(bits=4096)
    out2 = StringIO()
    key.write_private_key(out2)
    yield (out1.getvalue(), out2.getvalue())


@pytest.fixture()
def cache_entries(app):
    """Create sample cache entries."""
    data = [
        {
            'created': datetime.timestamp(datetime.now() -
                                          timedelta(days=7,
                                                    seconds=1)),
            'celery_id': str(uuid.uuid4()),
            'bucket_id': str(uuid.uuid4()),
            'protocol': 'sftp',
            'location': '/path/to/file1',
        },
        {
            'created': datetime.timestamp(datetime.now() -
                                          timedelta(days=6,
                                                    hours=23,
                                                    minutes=50)),
            'celery_id': str(uuid.uuid4()),
            'bucket_id': str(uuid.uuid4()),
            'protocol': 'sftp',
            'location': '/path/to/file2',
        },
        {
            'created': datetime.timestamp(datetime.now() - timedelta(days=6)),
            'celery_id': str(uuid.uuid4()),
            'bucket_id': str(uuid.uuid4()),
            'protocol': 'sftp',
            'location': '/path/to/file3',
        },
    ]
    yield data
    delete_all_keys()
