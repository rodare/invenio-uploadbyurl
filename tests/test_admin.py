# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Test uploadbyurl admin views."""

from flask import url_for
from flask_admin import Admin
from invenio_db import db

from invenio_uploadbyurl.admin import remoteserver_adminview, sshkey_adminview


def test_admin(app, remote):
    """Test uploadbyurl admin interface."""
    assert isinstance(remoteserver_adminview, dict)
    assert isinstance(sshkey_adminview, dict)

    assert 'model' in remoteserver_adminview
    assert 'modelview' in remoteserver_adminview
    assert 'model' in sshkey_adminview
    assert 'modelview' in sshkey_adminview

    admin = Admin(app, name="Test")

    remoteserver_model = remoteserver_adminview.pop('model')
    remoteserver_view = remoteserver_adminview.pop('modelview')
    admin.add_view(remoteserver_view(
        remoteserver_model, db.session,
        **remoteserver_adminview
    ))
    sshkey_model = sshkey_adminview.pop('model')
    sshkey_view = sshkey_adminview.pop('modelview')
    admin.add_view(sshkey_view(
        sshkey_model, db.session,
        **sshkey_adminview
    ))

    with app.app_context():
        rs_index_view_url = url_for('remoteserver.index_view')
        rs_delete_view_url = url_for('remoteserver.delete_view')
        key_index_view_url = url_for('sshkey.index_view')

    with app.test_client() as client:
        resp = client.get(
            rs_index_view_url,
            follow_redirects=True,
        )
        assert resp.status_code == 200
        assert 'foo' in str(resp.get_data())
        assert 'Server Address' in str(resp.get_data())

        resp = client.get(
            key_index_view_url,
            follow_redirects=True,
        )
        assert resp.status_code == 200
        assert 'foo' in str(resp.get_data())
        assert 'Keytype' in str(resp.get_data())
