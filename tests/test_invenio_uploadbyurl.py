# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Module tests."""

from unittest import mock

import pytest
from flask import Flask, url_for
from invenio_records.models import RecordMetadata
from invenio_records_files.api import RecordsBuckets

from invenio_uploadbyurl import InvenioUploadByURL
from invenio_uploadbyurl.links import default_uploadbyurl_link_factory


def test_version():
    """Test version import."""
    from invenio_uploadbyurl import __version__
    assert __version__


def test_init():
    """Test extension initialization."""
    app = Flask('testapp')
    ext = InvenioUploadByURL(app)
    assert 'invenio-uploadbyurl' in app.extensions

    app = Flask('testapp')
    ext = InvenioUploadByURL()
    assert 'invenio-uploadbyurl' not in app.extensions
    ext.init_app(app)
    assert 'invenio-uploadbyurl' in app.extensions


def test_link_factory_no_bucket(app):
    """Test without bucket."""
    assert default_uploadbyurl_link_factory(None) is None


def test_link_factory_with_bucket(app, db, bucket, remote):
    """Test with bucket."""
    with app.test_request_context():
        with db.session.begin_nested():
            record = RecordMetadata()
            RecordsBuckets.create(record, bucket)
            db.session.add(record)
        pid = mock.Mock()
        pid.get_assigned_object.return_value = record.id

        links = dict(
            uploadviaurl=url_for(
                'invenio_uploadbyurl.uploadbyurl_api',
                bucket_id=bucket.id,
                _external=True
            )
        )

        assert default_uploadbyurl_link_factory(pid) == links


def test_alembic(app, db):
    """Test alembic recipes."""
    ext = app.extensions['invenio-db']

    if db.engine.name == 'sqlite':
        raise pytest.skip('Upgrades are not supported on SQLite.')

    assert not ext.alembic.compare_metadata()
    db.drop_all()
    ext.alembic.upgrade()

    assert not ext.alembic.compare_metadata()
    ext.alembic.stamp()
    ext.alembic.downgrade(target='96e796392533')
    ext.alembic.upgrade()

    assert not ext.alembic.compare_metadata()
