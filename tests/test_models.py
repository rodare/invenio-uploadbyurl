# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Test SQLAlchemy models."""

from datetime import date, datetime, timedelta

import pytest
from sqlalchemy.exc import IntegrityError

from invenio_uploadbyurl.models import RemoteServer, SSHKey


def test_remote_server(app, db):
    """Test remote server model."""
    with db.session.begin_nested():
        remote1 = RemoteServer(
            name='test1',
            access_protocol='sftp',
            server_address='test1.hzdr.de')
        remote2 = RemoteServer(
            name='test2',
            access_protocol='sftp',
            server_address='test2.hzdr.de')
        remote3 = RemoteServer(
            name='test3',
            access_protocol='sftp',
            server_address='test3456768890034443.fz-rossendorf.de')
        db.session.add(remote1)
        db.session.add(remote2)
        db.session.add(remote3)

    db.session.commit()

    assert RemoteServer.get_by_name('test1').name == 'test1'
    assert RemoteServer.get_by_name('test2').name == 'test2'
    assert RemoteServer.get_by_name('test3').name == 'test3'
    assert RemoteServer.get_by_name(
        'test3').server_address == 'test3456768890034443.fz-rossendorf.de'

    assert len(RemoteServer.all()) == 3

    with pytest.raises(ValueError):
        with db.session.begin_nested():
            remote4 = RemoteServer(
                name='test4-$',
                access_protocol='sftp',
                server_address='test4.hzdr.de',
                description='Super remote server.')
            db.session.add(remote4)

    with pytest.raises(ValueError):
        with db.session.begin_nested():
            remote4 = RemoteServer(
                name='012345678901234567890',
                access_protocol='sftp',
                server_address='test4.hzdr.de')
            db.session.add(remote4)


def test_ssh_key_pair(app, db, user, sshkeys):
    """Test SSH key pair model."""
    # create remote server
    with db.session.begin_nested():
        remote1 = RemoteServer(
            name='test1',
            access_protocol='sftp',
            server_address='test1.hzdr.de')
        remote2 = RemoteServer(
            name='test2',
            access_protocol='sftp',
            server_address='test2.hzdr.de')
        db.session.add(remote1)
        db.session.add(remote2)

    db.session.commit()

    assert RemoteServer.get_by_name('test1').name == 'test1'

    # Create ssh key pair
    remote_server1 = RemoteServer.get_by_name('test1')
    remote_server2 = RemoteServer.get_by_name('test2')
    keypair = SSHKey.create(sshkeys[0], 'username', user, remote_server1)
    assert keypair.user.id == user.id
    keypair = SSHKey.create(sshkeys[0], 'username', user, remote_server2)
    assert keypair.remote_server.id == remote_server2.id

    db.session.commit()

    # test unique constraint
    with pytest.raises(IntegrityError):
        keypair = SSHKey.create(sshkeys[0], 'username', user, remote_server1)

    # test key retrieval
    key = SSHKey.get(user.id, remote_server1.id)
    assert key.user.id == user.id
    assert key.remote_server.id == remote_server1.id
    assert key.private_key == sshkeys[0]

    # test key update
    valid_until = datetime.now().date() + timedelta(days=10)
    SSHKey.update_key(
        user.id, remote1.id, sshkeys[1], valid_until=valid_until)
    db.session.commit()
    key = SSHKey.get(user.id, remote1.id)
    assert key.private_key == sshkeys[1]
    assert key.valid_until == valid_until

    # test key update without valid_until
    SSHKey.update_key(
        user.id, remote1.id, sshkeys[0])
    db.session.commit()
    key = SSHKey.get(user.id, remote1.id)
    assert key.private_key == sshkeys[0]
    assert key.valid_until == date.min
