# -*- coding: utf-8 -*-
#
# Copyright (C) 2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Test settings views."""

import json
import os
from unittest import mock

from flask import url_for
from testutils import MockAsyncResult, login_user

from invenio_uploadbyurl.models import SSHKey
from invenio_uploadbyurl.tasks import _redisstore
from invenio_uploadbyurl.utils import create_cache_key


@mock.patch('invenio_uploadbyurl.views.settings.AsyncResult')
@mock.patch('invenio_uploadbyurl.views.settings.url_for')
def test_index(mock_url, mock_ar, client, db, user, remote, cache_entries):
    """Test the index view."""
    # add jobs to REDIS
    _redisstore.put(
        create_cache_key(user.id),
        json.dumps(cache_entries).encode('utf-8'))
    mock_ar.return_value = MockAsyncResult()
    mock_url.return_value = '/deposit/new'

    url = url_for('invenio_uploadbyurl_settings.index')
    resp = client.get(url)
    # expect redirect to login form
    assert resp.status_code == 302
    assert '/login' in resp.location

    login_user(client, user)
    resp = client.get(url)
    assert resp.status_code == 200
    resp_text = resp.get_data(as_text=True)
    assert os.getenv('SFTP_SERVER') in resp_text
    assert 'Disconnect' in resp_text

    # check job status information
    assert cache_entries[0]['celery_id'] in resp_text
    assert cache_entries[0]['bucket_id'] in resp_text
    assert cache_entries[2]['location'] in resp_text

    SSHKey.delete(user.id, remote.id)
    db.session.commit()
    login_user(client, user)
    resp = client.get(url)
    assert resp.status_code == 200
    assert 'Connect' in resp.get_data(as_text=True)


def test_init(client, db, user, remote):
    """Test remote initialization view."""
    SSHKey.delete(user.id, remote.id)
    db.session.commit()

    init_url = url_for('invenio_uploadbyurl_settings.init', remote_name='foo')
    resp = client.get(init_url)
    # expect redirect to login form
    assert resp.status_code == 302
    assert '/login' in resp.location

    login_user(client, user)
    resp = client.get(init_url)
    assert resp.status_code == 200
    assert 'Connect to foo server:' in resp.get_data(as_text=True)

    # Test setup of connection
    resp = client.post(
        init_url,
        data=dict(
            username='foo',
            password='pass',
        ),
        follow_redirects=True,
    )
    assert resp.status_code == 200
    assert 'Disconnect' in resp.get_data(as_text=True)


def test_delete(client, db, user, remote):
    """Test delete view."""
    delete_url = url_for('invenio_uploadbyurl_settings.delete',
                         remote_name='foo')
    resp = client.get(delete_url)
    # expect redirect to login form
    assert resp.status_code == 302
    assert '/login' in resp.location

    login_user(client, user)

    resp = client.get(delete_url, follow_redirects=True)
    assert resp.status_code == 200
    assert 'Disconnect' not in resp.get_data(as_text=True)

    # Just don't do anything if connection is not set up before deletion
    resp = client.get(delete_url, follow_redirects=True)
    assert resp.status_code == 200
