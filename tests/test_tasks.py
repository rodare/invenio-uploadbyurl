# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Task tests."""

import hashlib
import json
from unittest import mock

import pexpect
import pytest
from celery.exceptions import OperationalError
from invenio_files_rest.helpers import compute_md5_checksum
from invenio_files_rest.models import ObjectVersion
from testutils import count_ubukeys

from invenio_uploadbyurl.tasks import _redisstore, clear_old_jobs, \
    clear_redisstore, download_files, download_via_sftp
from invenio_uploadbyurl.utils import create_cache_key


@mock.patch('invenio_uploadbyurl.tasks.create_objectversion_from_url')
def test_download_files(mock_createov, app, bucket, user):
    """Test download files celery task."""
    mock_createov.side_effect = OperationalError()
    app.config['UPLOADBYURL_NOTIFICATION_ENABLED'] = True
    with app.extensions['mail'].record_messages() as outbox:
        with pytest.raises(OperationalError):
            download_files.delay(
                bucket.id,
                user.id,
                'https://test.de/download.png',
            )
            assert len(outbox) == 1
            assert outbox[0].recipients == ['bucket@hzdr.de']
            assert outbox[0].subject == app.config[
                'UPLOADBYURL_EMAIL_TITLE_FAILED']
            assert outbox[0].body == app.config[
                'UPLOADBYURL_EMAIL_BODY_FAILED_URL'].format(
                    url='https://test.de/download.png')


def test_clear_redisstore(client):
    """Test clearing redis store."""
    # fill redisstore with sample values
    clear_redisstore.delay()
    pairs = [(134, 'test'), (1234, '5678'), (789, '1213')]
    for pair in pairs:
        key = create_cache_key(pair[0])
        _redisstore.put(key, pair[1].encode('utf-8'))
    assert count_ubukeys(_redisstore.keys()) == 3
    # clear redis
    clear_redisstore.delay()
    assert count_ubukeys(_redisstore.keys()) == 0


@mock.patch('invenio_uploadbyurl.tasks.delete_all_keys')
def test_clear_redisstore_error(mock_delete, client):
    """Test task retry."""
    mock_delete.side_effect = IOError()
    with pytest.raises(IOError):
        clear_redisstore.delay()


def test_sftp(client, db, user, user2, remote, bucket):
    """Test sftp connection."""
    # user with rsa key
    path = 'upload/README.rst'
    download_via_sftp.delay(bucket.id, remote.id, user.id, path)

    # user with ecdsa key
    path = 'upload/README.rst'
    download_via_sftp.delay(bucket.id, remote.id, user2[0].id, path)

    object_version = ObjectVersion.get(bucket=bucket.id, key='README.rst')
    file_instance = object_version.file
    with open('README.rst', 'rb') as fp:
        checksum = compute_md5_checksum(fp)
    assert file_instance.checksum == checksum

    path = '/upload/123456/test.data'
    with pytest.raises(IOError):
        download_via_sftp.delay(bucket.id, remote.id, user.id, path)


@pytest.mark.parametrize("user_list,mail_sent", [
    ([], True),
    ([1], False),
])
def test_sftp_with_mail(user_list, mail_sent, app, client, db,
                        user, remote, bucket):
    """Test sftp with activated notification."""
    app.config['UPLOADBYURL_NOTIFICATION_ENABLED'] = True
    app.config['UPLOADBYURL_USER_NOTIFICATIONS_OFF'] = user_list
    path = 'upload/README.rst'
    with app.extensions['mail'].record_messages() as outbox:
        download_via_sftp.delay(bucket.id, remote.id, user.id, path)
        # test if mail was sent
        if mail_sent:
            assert len(outbox) == 1
            assert outbox[0].recipients == ['bucket@hzdr.de']
            assert outbox[0].subject == app.config['UPLOADBYURL_EMAIL_TITLE']
            assert outbox[0].body == app.config['UPLOADBYURL_EMAIL_BODY']
        else:
            assert len(outbox) == 0


@mock.patch('invenio_uploadbyurl.tasks.pexpect.spawn')
def test_sftp_failure_with_mail(mock_expect, app, client,
                                db, user, remote, bucket):
    """Test failing upload with notifications enabled."""
    app.config['UPLOADBYURL_NOTIFICATION_ENABLED'] = True
    path = 'upload/README.rst'
    mock_expect.side_effect = pexpect.TIMEOUT('Timeout reached.')
    with pytest.raises(Exception):
        with app.extensions['mail'].record_messages() as outbox:
            download_via_sftp.delay(bucket.id, remote.id, user.id, path)
            # test if mail was sent
            assert len(outbox) == 1
            assert outbox[0].recipients == ['bucket@hzdr.de']
            assert outbox[0].subject == app.config[
                'UPLOADBYURL_EMAIL_TITLE_FAILED']
            assert outbox[0].body == app.config[
                'UPLOADBYURL_EMAIL_BODY_FAILED'].format(filepath=path)


@mock.patch('invenio_uploadbyurl.tasks.pexpect.spawn')
def test_sftp_failure(mock_expect, client, db, user, remote, bucket):
    """Test failing sftp download."""
    path = 'upload/README.rst'
    mock_expect.side_effect = pexpect.TIMEOUT('Timeout reached.')
    with pytest.raises(Exception):
        download_via_sftp.delay(bucket.id, remote.id, user.id, path)


def test_cache_cleanup(app, cache_entries):
    """Test the deletion of old cache entries."""
    _redisstore.put(
        app.config['UPLOADBYURL_KV_PREFIX'] + '#1',
        json.dumps(cache_entries).encode('utf-8'))
    clear_old_jobs.delay()
    data = _redisstore.get(app.config['UPLOADBYURL_KV_PREFIX'] + '#1')
    job_list = json.loads(data.decode('utf-8'))
    assert len(job_list) == 2

    # Key should be removed from Redis after cleanup
    app.config.update(UPLOADBYURL_CACHE_DURATION=1)
    clear_old_jobs.delay()
    with pytest.raises(KeyError):
        data = _redisstore.get(app.config['UPLOADBYURL_KV_PREFIX'] + '#1')
