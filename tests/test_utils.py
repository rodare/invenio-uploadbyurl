# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Test utility functions."""

import json
import os
import uuid
from io import StringIO
from unittest import mock

import paramiko
import pytest
from flask_login import current_user, login_user
from testutils import MockAsyncResult

from invenio_uploadbyurl.config import UPLOADBYURL_COMMENT
from invenio_uploadbyurl.models import RemoteServer, SSHKey
from invenio_uploadbyurl.tasks import _redisstore
from invenio_uploadbyurl.utils import connect_user_and_server, \
    create_cache_key, delete_ssh_key, deploy_ssh_key, generate_public_keystr, \
    generate_rsa_key, is_upload_active, notification_mail, \
    rebuild_private_keys


def test_deploy_key(app):
    """Test deployment function."""
    prv, pub = generate_rsa_key()
    server_name = os.getenv('SFTP_SERVER')
    deploy_ssh_key(pub, server_name, username='foo', password='pass', prv=prv)
    # test if login using private key is possible
    with paramiko.SSHClient() as client:
        client.load_system_host_keys()
        prv_str = StringIO(prv)
        rsa_key = paramiko.RSAKey.from_private_key(prv_str)
        client.connect(server_name, username='foo', pkey=rsa_key)


def test_connect_user(app, client, db, user2, remote):
    """Test connecting user and server."""
    with app.test_request_context():
        # Login user2[1]
        login_user(user=user2[1])
        assert current_user.is_authenticated
        connect_user_and_server(remote, 'foo', 'pass')
        # Check if key is in db
        key = SSHKey.get(user2[1].id, remote.id)
        assert key
        # check if key was entered successfully -> login should work
        with paramiko.SSHClient() as client:
            client.load_system_host_keys()
            prv_str = StringIO(key.private_key)
            rsa_key = paramiko.RSAKey.from_private_key(prv_str)
            client.connect(remote.server_address, username='foo',
                           pkey=rsa_key)
        # delete SSH key
        delete_ssh_key(remote)
        # connecting via SSH must fail
        with pytest.raises(paramiko.SSHException):
            with paramiko.SSHClient() as client:
                client.load_system_host_keys()
                prv_str = StringIO(key.private_key)
                rsa_key = paramiko.RSAKey.from_private_key(prv_str)
                client.connect(remote.server_address, username='foo',
                               pkey=rsa_key)
        # check if key is also removed from db
        key = SSHKey.get(user2[1].id, remote.id)
        assert not key


def test_generate_public_keystr(remote, user, user2):
    """Test generation of public key string."""
    # test rsa key
    rsa_key = SSHKey.get(user.id, remote.id)
    pub_key = generate_public_keystr(rsa_key)
    with open('tests/sftp/test-rsa.pub') as key_file:
        data = key_file.read().replace('\n', '')
    data += ' ' + UPLOADBYURL_COMMENT
    assert data == pub_key
    # test ecdsa key
    ecdsa_key = SSHKey.get(user2[0].id, remote.id)
    pub_key = generate_public_keystr(ecdsa_key)
    with open('tests/sftp/test-ecdsa.pub') as key_file:
        data = key_file.read().replace('\n', '')
    data += ' ' + UPLOADBYURL_COMMENT
    assert data == pub_key


def test_notification_mail(app, user):
    """Test notification mail sending."""
    with app.extensions['mail'].record_messages() as outbox:
        notification_mail(user.id)
        notification_mail(user.id, failed=True, filepath='/tmp/test')
        notification_mail(
            user.id, failed=True, url='https://www.test.de/test.zip')
        assert len(outbox) == 3
        assert outbox[0].recipients == ['bucket@hzdr.de']
        assert outbox[0].subject == app.config['UPLOADBYURL_EMAIL_TITLE']
        assert outbox[0].body == app.config['UPLOADBYURL_EMAIL_BODY']

        assert outbox[0].recipients == ['bucket@hzdr.de']
        assert outbox[1].subject == app.config[
            'UPLOADBYURL_EMAIL_TITLE_FAILED']
        assert outbox[1].body == app.config[
            'UPLOADBYURL_EMAIL_BODY_FAILED'].format(filepath='/tmp/test')

        assert outbox[0].recipients == ['bucket@hzdr.de']
        assert outbox[2].subject == app.config[
            'UPLOADBYURL_EMAIL_TITLE_FAILED']
        assert outbox[2].body == app.config[
            'UPLOADBYURL_EMAIL_BODY_FAILED_URL'].format(
                url='https://www.test.de/test.zip')


def test_rebuilding_privatekeys(app, db, remote):
    """Test rebuilding of private keys with random new SECRET_KEY."""
    old_key = app.secret_key

    # Get the secret key
    key = SSHKey.get(user_id=1, remote_server_id=1)

    app.secret_key = 'NEW_KEY_VALUE'
    db.session.expunge_all()

    # Assert that existing key cannot be read anymore
    with pytest.raises(ValueError):
        SSHKey.get(user_id=1, remote_server_id=1)

    rebuild_private_keys(old_key)

    key = SSHKey.query.first()

    # make sure the SSHKey is still the same
    assert SSHKey.get(user_id=1, remote_server_id=1) == key


@mock.patch('invenio_uploadbyurl.utils.AsyncResult')
def test_is_active(mock_ar, app, cache_entries):
    """Test check if background job is active."""
    assert not is_upload_active(0, uuid.uuid4())

    key = create_cache_key(0)
    _redisstore.put(key, json.dumps(cache_entries).encode('utf-8'))

    # Job already finished successfully
    mock_ar.return_value = MockAsyncResult()
    assert not is_upload_active(0, cache_entries[0]['bucket_id'])

    # Job is currently running
    mock_ar.return_value = MockAsyncResult(state='STARTED')
    assert is_upload_active(0, cache_entries[0]['bucket_id'])

    # Job is currently running, but belongs to different bucket
    assert not is_upload_active(0, uuid.uuid4())
