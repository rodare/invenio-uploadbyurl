# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Views tests."""

import json
import os
import uuid
from datetime import datetime
from unittest import mock

import httpretty
import pytest
from flask import Flask, url_for
from invenio_accounts.testutils import create_test_user
from invenio_db import db
from invenio_files_rest.models import Bucket
from testutils import MockAsyncResult, login_user

from invenio_uploadbyurl.api import _redisstore
from invenio_uploadbyurl.errors import FileDoesNotExist
from invenio_uploadbyurl.models import SSHKey
from invenio_uploadbyurl.utils import create_cache_key, generate_rsa_key


def test_proxy(app):
    """Test if redis proxy is accessible."""
    _redisstore.put('test', '1234'.encode('utf-8'))
    res = _redisstore.get('test').decode('utf-8')
    assert res == '1234'


@mock.patch('invenio_uploadbyurl.api.AsyncResult')
def test_get_api(mock_asyncresult, client, bucket, user):
    """Test get request."""
    mock_asyncresult.return_value = MockAsyncResult()
    login_user(client, user)
    url_get = url_for('invenio_uploadbyurl.uploadbyurl_api',
                      bucket_id=bucket.id)
    resp = client.get(url_get)
    assert resp.status_code == 404

    # add false sample ids to redisstore
    data = [dict(
        celery_id=str(uuid.uuid4()),
        bucket_id=str(bucket.id),
        location="/test/location/path",
        protocol="sftp",
        created=datetime.now().timestamp(),
    )]
    _redisstore.put(str(bucket.id), json.dumps(data).encode('utf-8'))
    resp = client.get(url_get)
    assert resp.status_code == 404

    # add correct sample id to redisstore
    key = create_cache_key(user.id)
    _redisstore.put(key, json.dumps(data).encode('utf-8'))
    resp = client.get(url_get)
    assert resp.status_code == 200

    # add second id
    data.append(dict(
        celery_id=str(uuid.uuid4()),
        bucket_id=str(bucket.id),
        location="/test/location/path2",
        protocol="sftp",
        created=datetime.now().timestamp(),
    ))
    _redisstore.put(key, json.dumps(data).encode('utf-8'))
    resp = client.get(url_get)
    assert resp.status_code == 200


def test_post_api(app, client, bucket, user, db):
    """Test post request."""
    login_user(client, user)
    url = url_for('invenio_uploadbyurl.uploadbyurl_api',
                  bucket_id=bucket.id,
                  url="https://www.google.de/about"
                  "/img/social/generic-feed.svg",
                  key="test.png")
    resp = client.post(url)
    key = create_cache_key(user.id)
    assert resp.status_code == 202
    assert key in _redisstore.keys()

    # test request without given key
    url = url_for('invenio_uploadbyurl.uploadbyurl_api',
                  bucket_id=bucket.id,
                  url="https://www.google.de/about"
                  "/img/social/generic-feed.svg")
    resp = client.post(url)
    assert resp.status_code == 202
    assert key in _redisstore.keys()
    data = _redisstore.get(key).decode('utf-8')
    ids = json.loads(data)
    assert len(ids) == 2

    # Test allowed protocols.
    url = url_for('invenio_uploadbyurl.uploadbyurl_api',
                  bucket_id=bucket.id,
                  url="http://www.hzdr.de/db/PicOri?pOid=52010",
                  key="gitlab.png")
    resp = client.post(url)
    assert resp.status_code == 400

    # Test result without given URL.
    url = url_for('invenio_uploadbyurl.uploadbyurl_api',
                  bucket_id=bucket.id,
                  key="gitlab.png")
    resp = client.post(url)
    assert resp.status_code == 400

    # Test with invalid URL
    url = url_for('invenio_uploadbyurl.uploadbyurl_api',
                  bucket_id=bucket.id,
                  url="https://khzysed.jkdjjksks.de/test.zip",
                  key="test.zip")
    resp = client.post(url)
    assert resp.status_code == 400
    assert (b'An error occured while connecting to the specified URL.'
            in resp.data)

    # update allowed protocols in order to not use https
    app.config.update(
        UPLOADBYURL_ALLOWED_PROTOCOLS=['http', 'https']
    )

    # set max file size of bucket
    bucket.quota_size = 1500
    db.session.add(bucket)
    db.session.commit()

    # Test with too large file
    with httpretty.enabled():
        httpretty.register_uri(
            httpretty.HEAD,
            'http://test.de/test.zip',
            body='a'*1500,
            adding_headers={
                'Content-Length': '1500',
            },
            status=200,
        )
        httpretty.register_uri(
            httpretty.HEAD,
            'http://test.de/test.zip',
            body='a'*1500,
            adding_headers={
                'Content-Length': '1500',
            },
            status=200,
        )
        url = url_for('invenio_uploadbyurl.uploadbyurl_api',
                      bucket_id=bucket.id,
                      url="http://test.de/test.zip",
                      key="test.zip")
        resp = client.post(url)
        assert resp.status_code == 400
        assert b'The given file is too large.' in resp.data


@mock.patch('invenio_uploadbyurl.api.AsyncResult')
def test_sftp_post(mock_ar, client, bucket, remote, user, user2, db):
    """Test API endpoint for sftp download."""
    login_user(client, user)
    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp',
                  bucket_id=bucket.id,
                  remote_server=remote.name,
                  path="/home/foo/upload/README.rst")
    resp = client.post(url)
    assert resp.status_code == 202
    assert bucket.size == os.path.getsize('README.rst')

    # Test status API
    mock_ar.return_value = MockAsyncResult()
    url = url_for('invenio_uploadbyurl.uploadbyurl_api',
                  bucket_id=bucket.id)
    resp = client.get(url)
    assert resp.status_code == 200
    job_status = json.loads(resp.get_data(as_text=True))
    assert len(job_status.keys()) == 1
    assert job_status[next(iter(job_status))]['state'] == 'SUCCESS'

    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp',
                  bucket_id=bucket.id,
                  remote_server=remote.name,
                  path="/home/foo/upload/README123.rst")
    resp = client.post(url)
    assert resp.status_code == 400
    assert b'The given file does not exist or you do not have ' \
        b'read permissions.' in resp.data

    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp',
                  bucket_id=bucket.id,
                  remote_server=remote.name,
                  path="/home/foo/upload")
    resp = client.post(url)
    assert resp.status_code == 400
    assert b'Please provide an absolute file path. Upload of ' \
        b'directories is not supported.' in resp.data

    # set max file size of bucket
    bucket.quota_size = 1500
    db.session.add(bucket)
    db.session.commit()

    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp',
                  bucket_id=bucket.id,
                  remote_server=remote.name,
                  path="/home/foo/upload/README.rst")
    resp = client.post(url)
    assert resp.status_code == 400
    assert b'The given file is too large. The default limit for a ' \
        b'dataset per record is 100 GiB. The default maximum file size is ' \
        b'50 GiB.' in resp.data

    # generate new SSH key without transferring it
    key = SSHKey.delete(user_id=user.id, remote_server_id=remote.id)
    prv, pub = generate_rsa_key()
    key = SSHKey.create(private_key=prv, username='foo',
                        user=user, remote_server=remote)
    db.session.commit()
    resp = client.post(url)
    assert resp.status_code == 400
    assert b'SSH Authentication failed. Probably the SSH Key is ' \
        b'not added to the remote server. Try to add it manually or ' \
        b'contact the support.' in resp.data

    # no bucket permissions
    login_user(client, user2[1])
    resp = client.post(url)
    assert resp.status_code == 404


def test_sftp_post_fail(client, bucket, remote, user, user2):
    # path not given
    login_user(client, user)

    # path not absolute
    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp',
                  bucket_id=bucket.id,
                  remote_server=remote.name,
                  path="upload/README.rst")
    resp = client.post(url)
    assert resp.status_code == 400
    assert b'Please provide an absolute path ' \
        b'(e.g. /bigdata/rz/test.jpg).' in resp.data

    # unavailable remote server
    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp',
                  bucket_id=bucket.id,
                  remote_server='test1234',
                  path="/home/foo/upload/README.rst")
    resp = client.post(url)
    assert resp.status_code == 404
    assert b'Remote Server not found.' in resp.data

    # not connected yet
    SSHKey.delete(user.id, remote.id)
    db.session.commit()
    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp',
                  bucket_id=bucket.id,
                  remote_server=remote.name,
                  path="/home/foo/upload/README.rst")
    resp = client.post(url)
    assert resp.status_code == 400
    assert b'Please connect your account with ' \
        b'the remote server first.' in resp.data

    # no path given
    db.session.commit()
    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp',
                  bucket_id=bucket.id,
                  remote_server=remote.name)
    resp = client.post(url)
    assert resp.status_code == 400
    assert b'You need to specify an absolute file path.' in resp.data


def test_sftp_browse(client, remote, user):
    """Test get API call."""
    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp_browse',
                  remote_server=remote.name)
    resp = client.post(url)
    assert resp.status_code == 401

    login_user(client, user)
    resp = client.post(url)
    assert resp.status_code == 200

    node = dict(
        endpoint=url_for(
            'invenio_uploadbyurl.uploadbyurl_api_sftp_browse',
            remote_server=remote.name,
            _external=True,
        ),
        path='/',
        short_path='..',
        isdir=True,
    )
    assert node not in json.loads(resp.data.decode())

    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp_browse',
                  remote_server=remote.name,
                  path="/home/foo/upload")
    resp = client.post(url)
    assert resp.status_code == 200
    node = dict(
        endpoint=url_for(
            'invenio_uploadbyurl.uploadbyurl_api_sftp_browse',
            remote_server=remote.name,
            path='/home/foo',
            _external=True,
        ),
        path='/home/foo',
        short_path='..',
        isdir=True,
    )
    assert node in json.loads(resp.data.decode())

    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp_browse',
                  remote_server=remote.name,
                  path="/home/foo/upload")
    # generate new SSH key without transferring it
    key = SSHKey.delete(user_id=user.id, remote_server_id=remote.id)
    prv, pub = generate_rsa_key()
    key = SSHKey.create(private_key=prv, username='foo',
                        user=user, remote_server=remote)
    db.session.commit()
    resp = client.post(url)
    assert resp.status_code == 400
    assert b'SSH Authentication failed. Probably the SSH Key is ' \
        b'not added to the remote server. Try to add it manually or ' \
        b'contact the support.' in resp.data


def test_sftp_browse_fail(client, remote, user):
    """Test failing get API call."""
    login_user(client, user)
    # path not absolute
    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp_browse',
                  remote_server=remote.name,
                  path="upload/README.rst")
    resp = client.post(url)
    assert resp.status_code == 400
    assert b'Please provide an absolute path ' \
        b'(e.g. /bigdata/rz/test.jpg).' in resp.data

    # unavailable remote server
    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp_browse',
                  remote_server='test1234',
                  path="/home/foo/upload")
    resp = client.post(url)
    assert resp.status_code == 404
    assert b'Remote Server not found.' in resp.data

    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp_browse',
                  remote_server=remote.name,
                  path="/home/foo/upload/12345")
    resp = client.post(url)
    assert resp.status_code == 400
    assert b'Please provide a valid directory path.' in resp.data

    # not connected yet
    SSHKey.delete(user.id, remote.id)
    db.session.commit()
    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp_browse',
                  remote_server=remote.name,
                  path="/home/foo/upload")
    resp = client.post(url)
    assert resp.status_code == 400
    assert b'Please connect your account with ' \
        b'the remote server first.' in resp.data

    url = url_for('invenio_uploadbyurl.uploadbyurl_api_sftp_browse',
                  remote_server=remote.name)
