# -*- coding: utf-8 -*-
#
# Copyright (C) 2017-2018 HZDR
#
# This file is part of Rodare.
#
# Rodare is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Rodare is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Rodare.  If not, see <http://www.gnu.org/licenses/>.

"""Test utilities."""

from io import StringIO

import paramiko
from flask import current_app

from invenio_uploadbyurl.models import SSHKey


def login_user(client, user):
    """Log in a specified user."""
    with client.session_transaction() as sess:
        sess['user_id'] = user.id if user else None
        sess['_fresh'] = True


def count_ubukeys(key_list):
    """Counts uploadbyurl keys in redis."""
    return sum(
        1 for value in key_list if value.startswith(
            current_app.config['UPLOADBYURL_KV_PREFIX']))


class MockAsyncResult():
    """Mocking class for celery AsyncResult."""

    def __init__(self, state='SUCCESS'):
        """Mock initialization."""
        self.state = state
